# Pot Engine #

Pot Engine is a lightweight C++ game engine developed in C++, based on:

* SDL2
* OpenGL
* Apache XML Xerces
* Boost system

Pot Engine is implemented as a single shared library (libpot.so). It is a personal project.

# Dependencies #

The following library dependencies are needed for Pot Engine to compile:

* libsdl2-dev
* libglew-dev
* libgl1-mesa-dev
* libglm-dev
* libxerces-c-dev
* libboost-system-dev

-l SDL2 GLEW GL xerces-c boost_system

# Media #

This video shows the dynamic octree data structure that I implemented to manage space partitioning and collision detection: 
https://www.youtube.com/watch?v=ujJsK5903AA