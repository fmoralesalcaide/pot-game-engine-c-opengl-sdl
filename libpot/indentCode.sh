#!/bin/bash

# Format files
FILES="src/*.cpp include/*.h"
astyle --style=allman --recursive --mode=c --lineend=linux $FILES

# Delete backup files
find . -type f -name '*.orig' -delete