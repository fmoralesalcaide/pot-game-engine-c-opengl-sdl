#pragma once

#include <Pot/Generic/GenericDefinitions.h>
#include <Pot/Components/Component.h>
#include <Pot/IComponentReceivers/IUpdateReceiver.h>
#include <Pot/IComponentReceivers/ICollisionReceiver.h>

namespace Pot
{

namespace Components
{

class Solid: public Component, public IUpdateReceiver, public ICollisionReceiver
{
public:
    Solid(GameObject* gameObject, uint32_t id, const std::string& name = "Rotator");
    virtual ~Solid();

    void onUpdate(double deltaTime);
    void onCollisionEnter(const CollisionInfo& info);
    void onCollisionStay(const CollisionInfo& info);
    void onCollisionLeave();

    bool hasGravity() const;
    bool canImpact() const;
    double getMass() const;

    const glm::vec3& getGravityAccel() const;
    const glm::vec3& getImpactAccel() const;
    glm::vec3 getTotalAccel() const;
    const glm::vec3& getSpeed() const;

    void setHasGravity(bool hasGravity);
    void setCanImpact(bool canImpact);

    void setMass(double mass);
    void setGravityAccel(const glm::vec3& gravity);

private:
    bool receivesGravity;
    bool receivesImpacts;
    double mass;

    glm::vec3 gravityAccel;
    glm::vec3 impactAccel;
    glm::vec3 speed;
};

}

}
