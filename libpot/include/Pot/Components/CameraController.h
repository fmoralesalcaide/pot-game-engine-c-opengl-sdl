#pragma once

#include <Pot/Components/Component.h>
#include <Pot/IComponentReceivers/IUpdateReceiver.h>
#include <Pot/IComponentReceivers/IKeyboardReceiver.h>
#include <Pot/IComponentReceivers/IMouseReceiver.h>

namespace Pot
{

namespace Components
{

class CameraController : public Pot::Component, public Pot::IUpdateReceiver, public Pot::IKeyboardReceiver, public Pot::IMouseReceiver
{
public:
    CameraController(Pot::GameObject* gameObject, uint32_t id, const std::string& name = "CameraController");
    virtual ~CameraController();

    void initialize();
    void finish();

    void onUpdate(double deltaTime);
    void onKeyUp();
    void onKeyDown();
    void onMouseMove();

    void setSpeed(float speed);

private:
    bool navigating;
    float transSpeed;
    float rotSpeed;
    glm::vec3 direction;

    void checkNavigation();
    void recomputeDirection();
};

}

}
