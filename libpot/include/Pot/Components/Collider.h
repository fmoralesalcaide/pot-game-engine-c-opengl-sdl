#pragma once

#include <Pot/Components/Component.h>

namespace Pot
{

namespace Components
{

class Collider : public Component
{
public:
    enum class Type
    {
        PLANE,
        SPHERE
    };

    Collider(Type type, GameObject* gameObject, uint32_t id, const std::string& name = "Collider");
    virtual ~Collider();

    bool isUniqueType();

    inline Type getType() const
    {
        return type;
    };

    void setRelativePosition(const glm::vec3& relativePos);
    glm::vec3 getWorldPosition();

protected:
    Type type;
    glm::vec3 relativePos;
};

}

}
