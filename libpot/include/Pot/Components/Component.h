#pragma once

#include <Pot/Generic/GenericDefinitions.h>

namespace Pot
{
class GameObject;

class Component : public boost::noncopyable
{
public:
    Component(GameObject* gameObject, uint32_t id, const std::string& name);
    virtual ~Component();

    inline const uint32_t& getId() const
    {
        return id;
    };
    inline const std::string& getName() const
    {
        return name;
    };
    inline GameObject* getGameObject() const
    {
        return gameObject;
    };

    virtual bool isUniqueType();
    virtual void initialize();
    virtual void finish();

protected:
    uint32_t id;
    std::string name;
    GameObject* gameObject;
};
}
