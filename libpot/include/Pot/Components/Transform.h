#pragma once

#include <Pot/Components/Component.h>

namespace Pot
{

namespace Components
{

class Transform : public Component
{
public:
    Transform(GameObject* gameObject, uint32_t id, const std::string& name = "Transform");
    virtual ~Transform();

    void initialize();

    glm::vec3 scale;
    glm::vec3 localRot;
    glm::vec3 localPos;

    //void lookAt(const glm::vec3& target);
    void rotateLocally(const glm::vec3& v);
    void translateLocally(const glm::vec3& v);

    const glm::mat4& getViewMatrix();
    const glm::mat4& getRotTransMatrix();
    const glm::mat4& getModelMatrix();

    glm::mat4 getWorldMatrix(bool includeScaleMatrix = false);
    glm::vec3 getWorldPosition();

private:
    glm::vec3 lastScale;
    glm::vec3 lastRot;
    glm::vec3 lastPos;

    glm::mat4 scaleMatrix;
    glm::mat4 rotMatrix;
    glm::mat4 transMatrix;

    glm::mat4 rotTransMatrix;
    glm::mat4 viewMatrix;

    glm::mat4 modelMatrix;

    void recomputeMatrices();
};

}

}
