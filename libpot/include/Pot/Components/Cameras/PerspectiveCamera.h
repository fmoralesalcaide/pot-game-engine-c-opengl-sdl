#pragma once

#include <Pot/Components/Camera.h>

namespace Pot
{

namespace Components
{

class PerspectiveCamera : public Camera
{
public:
    PerspectiveCamera(GameObject* gameObject, uint32_t id, const std::string& name = "PerspectiveCamera");
    virtual ~PerspectiveCamera();

    inline const double& getFieldOfView() const
    {
        return fieldOfView;
    };
    inline const double& getAspectRatio() const
    {
        return aspectRatio;
    };
    inline const double& getNearClippingPlane() const
    {
        return nearClippingPlane;
    };
    inline const double& getFarClippingPlane() const
    {
        return farClippingPlane;
    };

    void setFieldOfView(double fieldOfView);
    void setAspectRatio(double aspectRatio);
    void setNearClippingPlane(double nearClippingPlane);
    void setFarClippingPlane(double farClippingPlane);

protected:
    double fieldOfView;
    double aspectRatio;
    double nearClippingPlane;
    double farClippingPlane;

    glm::mat4 computeProjectionMatrix();
};

}

}
