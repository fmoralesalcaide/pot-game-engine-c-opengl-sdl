#pragma once

#include <Pot/IComponentReceivers/IRenderReceiver.h>
#include <Pot/Components/Component.h>
#include <Pot/Resources/Resource.h>

namespace Pot
{

namespace Components
{

class MeshRenderer : public Component, public IRenderReceiver
{
public:
    MeshRenderer(GameObject* gameObject, uint32_t id, const std::string& name = "MeshRenderer");
    virtual ~MeshRenderer();

    virtual void onRender();

    void setShader(const std::string& shaderName);
    void setMesh(const std::string& name);

private:
    bool hasShaderAttached;
    Resource_sptr mesh;

    GLuint shaderProgramId;
};

}

}
