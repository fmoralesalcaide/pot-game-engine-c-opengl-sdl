#pragma once

#include <Pot/Components/Component.h>

namespace Pot
{

namespace Components
{

class Camera : public Component
{
public:
    enum class Type
    {
        PERSPECTIVE,
        ORTHOGRAPHIC
    };

    Camera(Type type, GameObject* gameObject, uint32_t id, const std::string& name = "Camera");
    virtual ~Camera();

    bool isUniqueType();

    inline Type getType() const
    {
        return type;
    };

    void setActive();
    void updateViewProjMatrix();

    inline const glm::mat4& getViewProjMatrix() const
    {
        return viewProjMatrix;
    };

    // View matrix
    glm::vec3 target;

protected:
    Type type;

    // View matrix is transform trans*rot matrix

    // Projection matrix
    bool recomputeProjMatrix;
    glm::mat4 projMatrix;

    virtual glm::mat4 computeProjectionMatrix() = 0;

private:
    // View-projection matrix
    glm::mat4 viewProjMatrix;
};

}

}
