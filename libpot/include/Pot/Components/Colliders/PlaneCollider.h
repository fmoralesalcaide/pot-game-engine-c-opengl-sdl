#pragma once

#include <Pot/Components/Collider.h>

namespace Pot
{

namespace Components
{

class PlaneCollider : public Collider
{
public:
    PlaneCollider(GameObject* gameObject, uint32_t id, const std::string& name = "PlaneCollider");
    virtual ~PlaneCollider();

private:
    glm::vec3 normal;
};

}

}
