#pragma once

#include <Pot/Components/Collider.h>

namespace Pot
{

namespace Components
{

class SphereCollider : public Collider
{
public:
    SphereCollider(GameObject* gameObject, uint32_t id, const std::string& name = "SphereCollider");
    virtual ~SphereCollider();

    inline double getRadius() const
    {
        return radius;
    };
    void setRadius(double radius);

private:
    double radius;
};

}

}
