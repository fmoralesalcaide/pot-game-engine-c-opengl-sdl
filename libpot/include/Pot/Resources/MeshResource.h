#pragma once

#include <Pot/Resources/Resource.h>

namespace Pot
{

class MeshResource : public Resource
{
public:
    MeshResource(
        uint32_t id,
        const std::string& name,
        const std::vector<GLfloat>& positions,
        const std::vector<GLfloat>& normals,
        const std::vector<GLfloat>& uvs,
        const std::vector<GLuint>& indices,
        GLuint offsetInBuffer
    );

    virtual ~MeshResource();

    uint32_t getId() const;

    const std::vector<GLfloat>& getPositions() const;
    const std::vector<GLfloat>& getNormals() const;
    const std::vector<GLfloat>& getUVs() const;
    const std::vector<GLuint>& getIndices() const;

    GLuint getOffsetInBuffer() const;

private:
    // Mesh info
    std::vector<GLfloat> positions;
    std::vector<GLfloat> normals;
    std::vector<GLfloat> uvs;
    std::vector<GLuint> indices;

    // VBO
    GLuint offsetInBuffer;
};

}
