#pragma once

#include <Pot/Generic/GenericDefinitions.h>
#include <Pot/Resources/Resource.h>

namespace Pot
{

class ResourceFactory : public boost::noncopyable
{
public:
    ResourceFactory(Resource::Type type);
    virtual ~ResourceFactory();

    virtual Resource_sptr loadResource(uint32_t id, const std::string& name, const std::string& fileName) = 0;

protected:
    Resource::Type type;
};

}
