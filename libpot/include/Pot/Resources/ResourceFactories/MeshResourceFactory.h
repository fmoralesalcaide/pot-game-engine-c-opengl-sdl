#pragma once

#include <Pot/Resources/ResourceFactories/ResourceFactory.h>

namespace Pot
{

class MeshResourceFactory : public ResourceFactory
{
public:
    MeshResourceFactory();
    virtual ~MeshResourceFactory();

    Resource_sptr loadResource(uint32_t id, const std::string& name, const std::string& fileName);
};

}
