#pragma once

#include <Pot/Generic/GenericDefinitions.h>

namespace Pot
{

class Resource : public boost::noncopyable
{
public:
    enum class Type
    {
        MESH
    };

    virtual ~Resource();

    inline uint32_t getId() const
    {
        return id;
    }
    inline const std::string& getName() const
    {
        return name;
    }
    inline Type getType() const
    {
        return type;
    }

protected:
    uint32_t id;
    Type type;
    std::string name;

    Resource(uint32_t id, Type type, const std::string& name);

};

typedef std::shared_ptr<Resource> Resource_sptr;

}
