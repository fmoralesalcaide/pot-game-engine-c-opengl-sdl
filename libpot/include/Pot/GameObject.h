#pragma once

#include <Pot/Components/Component.h>
#include <Pot/Generic/StringTools.h>

namespace Pot
{
class GameObject : public boost::noncopyable
{
public:
    GameObject(uint32_t id);
    virtual ~GameObject();

    inline const uint32_t& getId() const
    {
        return id;
    };

    /** Hierarchy-related **/

    bool hasParent();
    bool hasChild(const uint32_t& id);
    bool hasChildren();

    GameObject* getParent();
    GameObject* getChild(const uint32_t& id);
    std::vector<GameObject*> getChildren();

    void detachFromParent();
    void detachChild(const uint32_t& id);
    void detachChildren();

    void attachToParent(GameObject* newParent);
    void attachChild(GameObject* newChild);

    void destroy();

    /** Component-related **/

    // Checks if there is a component of type T in the GameObject, optionally with a name.
    template <typename T>
    bool hasComponent(const std::string& name = std::string()) const;

    // Returns a component of type T in the GameObject, optionally with a name.
    template <typename T>
    T* getComponent(const std::string& name = std::string()) const;

    // Returns a vector containing all the components of type T in the GameObject.
    template <typename T>
    std::vector<T*> getComponents() const;

    // Returns all the components
    std::vector<Component*> getAllComponents() const;

    // Returns a vector containing all the components in the GameObject.
    std::vector<size_t> getComponentTypeIds() const;

    // The GameObject is notified of a new added Component of type T.
    template <typename T>
    void notifyComponentAdded(Component* component);

    // The GameObject is notified of a removed Component of type T.
    template <typename T>
    void notifyComponentRemoved(Component* component);

    // The GameObject is notified of all its Components were removed.
    void notifyAllComponentsRemoved();

private:
    uint32_t id;

    GameObject* parent;
    std::unordered_map<uint32_t, GameObject*> children;

    std::unordered_map<size_t, std::list<uint32_t>> componentsByType;
    std::unordered_map<uint32_t, Component*> components;
};

template <typename T>
bool GameObject::hasComponent(const std::string& name) const
{
    if(!std::is_base_of<Component, T>::value)
        THROW_EXCEPTION("Class " << Pot::StringTools::typeName(typeid(T)) << " is not a Component of GameObject " << id);

    size_t componentTypeId = typeid(T).hash_code();

    if(!componentsByType.count(componentTypeId))
        return false;
    else if(name.empty())
        return true;
    else
    {
        for(const uint32_t& componentId : componentsByType.at(componentTypeId))
        {
            Component* component = components.at(componentId);
            if(component->getName() == name)
                return true;
        }
    }

    return false;
}

template <typename T>
T* GameObject::getComponent(const std::string& name) const
{
    if(!hasComponent<T>(name))
        THROW_EXCEPTION("GameObject " << id << " does not have Components of type " << Pot::StringTools::typeName(typeid(T)));

    size_t componentTypeId = typeid(T).hash_code();
    T* result = nullptr;

    if(name.empty())
    {
        const uint32_t& componentId = componentsByType.at(componentTypeId).front();
        result = (T*) components.at(componentId);
    }
    else
    {
        for(const uint32_t& componentId : componentsByType.at(componentTypeId))
        {
            Component* component = components.at(componentId);
            if(component->getName() == name)
            {
                result = (T*) component;
                break;
            }
        }
    }

    if(!result)
        THROW_EXCEPTION("Could not find requested Component");

    return result;
}

template <typename T>
std::vector<T*> GameObject::getComponents() const
{
    if(!hasComponent<T>())
        THROW_EXCEPTION("GameObject " << id << " does not have Components of type " << Pot::StringTools::typeName(typeid(T)));

    size_t componentTypeId = typeid(T).hash_code();
    std::vector<T*> components;

    for(const uint32_t& componentId : componentsByType.at(componentTypeId))
    {
        components.push_back((T*) components.at(componentId));
    }

    return components;
}

template <typename T>
void GameObject::notifyComponentAdded(Component* component)
{
    size_t typeId = typeid(T).hash_code();
    const uint32_t& id = component->getId();

    if(!componentsByType.count(typeId))
        componentsByType[typeId];

    componentsByType.at(typeId).push_back(id);
    components[id] = component;
}

template <typename T>
void GameObject::notifyComponentRemoved(Component* component)
{
    size_t typeId = typeid(T).hash_code();
    const uint32_t& id = component->getId();

    componentsByType.at(typeId).remove(id);
    if(componentsByType.at(typeId).empty())
        componentsByType.erase(typeId);

    components.erase(id);
}
}
