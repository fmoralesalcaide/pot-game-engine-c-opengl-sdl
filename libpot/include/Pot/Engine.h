#pragma once

#include <Pot/Generic/GenericDefinitions.h>
#include <Pot/Managers/InputManager.h>
#include <Pot/Managers/CollisionManager.h>
#include <Pot/Managers/RenderManager.h>
#include <Pot/Managers/ResourceManager.h>
#include <Pot/Managers/UpdateManager.h>
#include <Pot/Managers/CameraManager.h>
#include <Pot/Managers/ComponentManager.h>
#include <Pot/Managers/GameObjectManager.h>

namespace Pot
{
class Engine
{
public:
    static Engine& getInstance()
    {
        static Engine instance;
        return instance;
    };
    Engine(Engine const&) = delete;
    void operator=(Engine const&) = delete;

    void initialize();
    void run();
    void stop();
    void finish();

    // Manager access
    inline InputManager& getInputManager()
    {
        return inputMgr;
    };
    inline CollisionManager& getCollisionManager()
    {
        return collisionMgr;
    };
    inline RenderManager& getRenderManager()
    {
        return renderMgr;
    };
    inline ResourceManager& getResourceManager()
    {
        return resourceMgr;
    };
    inline CameraManager& getCameraManager()
    {
        return cameraMgr;
    };
    inline UpdateManager& getUpdateManager()
    {
        return updateMgr;
    };
    inline GameObjectManager& getGameObjectManager()
    {
        return gameObjectMgr;
    };
    inline ComponentManager& getComponentManager()
    {
        return componentMgr;
    };

private:
    bool running;

    InputManager inputMgr;
    CollisionManager collisionMgr;
    RenderManager renderMgr;
    ResourceManager resourceMgr;
    CameraManager cameraMgr;
    UpdateManager updateMgr;
    GameObjectManager gameObjectMgr;
    ComponentManager componentMgr;

    Engine();
};

}
