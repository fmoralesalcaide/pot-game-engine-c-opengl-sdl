#pragma once

#include <Pot/IComponentReceiver.h>

namespace Pot
{

class IMouseReceiver : public IComponentReceiver
{
public:
    IMouseReceiver(uint32_t componentId);
    virtual ~IMouseReceiver();

    virtual void onMouseUp();
    virtual void onMouseDown();
    virtual void onMouseMove();
    virtual void onMouseScroll();
    // onMouseDrag()

};

}
