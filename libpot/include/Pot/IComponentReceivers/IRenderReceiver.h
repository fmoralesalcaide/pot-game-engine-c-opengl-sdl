#pragma once

#include <Pot/IComponentReceiver.h>

namespace Pot
{

class IRenderReceiver : public IComponentReceiver
{
public:
    IRenderReceiver(uint32_t componentId);
    virtual ~IRenderReceiver();

    virtual void onRender();
};

}
