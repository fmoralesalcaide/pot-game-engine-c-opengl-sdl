#pragma once

#include <Pot/IComponentReceiver.h>

namespace Pot
{

class IUpdateReceiver : public IComponentReceiver
{
public:
    IUpdateReceiver(uint32_t componentId);
    virtual ~IUpdateReceiver();

    virtual void onUpdate(double deltaTime);
};

}
