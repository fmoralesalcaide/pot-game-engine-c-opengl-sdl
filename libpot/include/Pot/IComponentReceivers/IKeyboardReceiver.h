#pragma once

#include <Pot/IComponentReceiver.h>

namespace Pot
{

class IKeyboardReceiver : public IComponentReceiver
{
public:
    IKeyboardReceiver(uint32_t componentId);
    virtual ~IKeyboardReceiver();

    virtual void onKeyUp();
    virtual void onKeyDown();

};

}
