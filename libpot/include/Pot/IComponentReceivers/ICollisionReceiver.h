#pragma once

#include <Pot/IComponentReceiver.h>
#include <Pot/Managers/CollisionManager/CollisionInfo.h>

namespace Pot
{

class ICollisionReceiver : public IComponentReceiver
{
public:
    ICollisionReceiver(uint32_t componentId, uint32_t gameObjectId);
    virtual ~ICollisionReceiver();

    virtual void onCollisionEnter(const CollisionInfo& info);
    virtual void onCollisionStay(const CollisionInfo& info);
    virtual void onCollisionLeave();

private:
    uint32_t gameObjectId;
};

}
