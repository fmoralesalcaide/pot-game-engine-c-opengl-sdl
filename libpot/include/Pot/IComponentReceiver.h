#pragma once

#include <Pot/Generic/GenericDefinitions.h>

namespace Pot
{
class IComponentReceiver : public boost::noncopyable
{
public:
    virtual ~IComponentReceiver();

protected:
    uint32_t componentId;

    IComponentReceiver(uint32_t componentId);
};
}
