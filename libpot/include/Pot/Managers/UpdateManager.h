#pragma once

#include <Pot/IComponentReceivers/IUpdateReceiver.h>
#include <Pot/Managers/Manager.h>

namespace Pot
{
class UpdateManager : Manager
{
public:
    UpdateManager();
    virtual ~UpdateManager();

    void initialize();
    void finish();

    void update(double deltaTime);

    void registerReceiver(uint32_t id, IUpdateReceiver* receiver);
    void unregisterReceiver(uint32_t id);

private:
    std::map<uint32_t, IUpdateReceiver*> receivers;
};
}
