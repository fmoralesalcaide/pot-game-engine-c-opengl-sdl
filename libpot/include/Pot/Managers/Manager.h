#pragma once

#include <Pot/Generic/GenericDefinitions.h>

namespace Pot
{
class Manager : public boost::noncopyable
{
public:
    enum class Type
    {
        INPUT,
        RENDER,
        CAMERA,
        UPDATE,
        COMPONENT,
        GAMEOBJECT,
        RESOURCE,
        COLLISION
    };

    Manager(Type type);
    virtual ~Manager();

    virtual void initialize();
    virtual void finish();

private:
    Type type;
};
}
