#pragma once

#include <Pot/GameObject.h>
#include <Pot/Managers/Manager.h>

namespace Pot
{

class GameObjectManager : public Manager
{
public:
    GameObjectManager();
    virtual ~GameObjectManager();

    void initialize();
    void finish();

    GameObject* instantiateGameObject();

    void addToDeletionList(GameObject* gameObject);
    void processDeletionList();

private:
    uint32_t nextGameObjectId;
    std::unordered_map<uint32_t, GameObject*> gameObjects;
    std::unordered_set<uint32_t> gameObjectsToRemove;
};

}
