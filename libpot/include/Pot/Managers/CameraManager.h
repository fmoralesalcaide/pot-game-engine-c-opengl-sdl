#pragma once

#include <Pot/Components/Camera.h>
#include <Pot/Managers/Manager.h>

namespace Pot
{

class CameraManager : public Manager
{
public:
    CameraManager();
    virtual ~CameraManager();

    virtual void finish();

    bool hasActiveCamera() const;
    Components::Camera* getActiveCamera();
    void setActiveCamera(uint32_t componentId);
    void unsetActiveCamera();

    void process();

    void registerCamera(uint32_t id, Components::Camera* camera);
    void unregisterCamera(uint32_t id);

private:
    Components::Camera* activeCamera;
    std::map<uint32_t, Components::Camera*> cameras;
};

}
