#pragma once

#include <Pot/Components/Component.h>
#include <Pot/GameObject.h>
#include <Pot/Managers/Manager.h>
#include <Pot/Generic/StringTools.h>

namespace Pot
{

class ComponentManager : public Manager
{
public:
    ComponentManager();
    virtual ~ComponentManager();

    void initialize();
    void finish();

    // Performs checks and adds a new Component of type T to the specified GameObject.
    template <typename T>
    T* addComponent(GameObject* parentGameObject, const std::string& name = std::string());

    // Performs checks and removes the Component of type T in the specified GameObject.
    template <typename T>
    void removeComponent(GameObject* parentGameObject, const std::string& name = std::string());

    // Removes all types of Component in the GameObject.
    void removeAllComponents(GameObject* parentGameObject);

    // Return the component with the given id
    Component* getComponent(uint32_t id);

private:
    uint32_t nextComponentId;
    std::unordered_map<uint32_t, Component*> components;

};

template <typename T>
T* ComponentManager::addComponent(GameObject* parentGameObject, const std::string& name)
{
    if(!parentGameObject)
        THROW_EXCEPTION("Cannot add Component to a null GameObject");
    if(parentGameObject->hasComponent<T>())
    {
        Component* component = parentGameObject->getComponent<T>();
        if(component->isUniqueType())
            THROW_EXCEPTION("Cannot add twice unique Component of type " << Pot::StringTools::typeName(typeid(T)) << "to GameObject " << parentGameObject->getId());
    }

    T* addedComponent = nullptr;

    if(name.empty())
        addedComponent = new T(parentGameObject, nextComponentId++); // Use default component name
    else
        addedComponent = new T(parentGameObject, nextComponentId++, name); // Use custom name

    parentGameObject->notifyComponentAdded<T>(addedComponent);
    components[addedComponent->getId()] = addedComponent;
    addedComponent->initialize();

    return addedComponent;
}

template <typename T>
void ComponentManager::removeComponent(GameObject* parentGameObject, const std::string& name)
{
    if(!parentGameObject)
        THROW_EXCEPTION("Cannot add Component to a null GameObject");
    if(!parentGameObject->hasComponent<T>(name))
        THROW_EXCEPTION("Cannot remove un-existsing Component of type " << Pot::StringTools::typeName(typeid(T)) << " from GameObject");

    Component* removedComponent = parentGameObject->getComponent<T>(name);
    removedComponent->finish();
    parentGameObject->notifyComponentRemoved<T>(removedComponent);
    uint32_t id = removedComponent->getId();
    delete components.at(id);
    components.erase(id);
}
}
