#pragma once

#include <Pot/IComponentReceivers/IMouseReceiver.h>
#include <Pot/IComponentReceivers/IKeyboardReceiver.h>
#include <Pot/Managers/Manager.h>

namespace Pot
{

class InputManager : public Manager
{
public:
    InputManager();
    virtual ~InputManager();

    void initialize();
    void finish();

    void processInput();

    void registerMouseReceiver(uint32_t id, IMouseReceiver* receiver);
    void unregisterMouseReceiver(uint32_t id);

    void registerKeyboardReceiver(uint32_t id, IKeyboardReceiver* receiver);
    void unregisterKeyboardReceiver(uint32_t id);

    bool isKeyPressed(const SDL_Scancode& key) const;
    std::tuple<int,int> getMousePos() const;
    std::tuple<int,int> getMouseDelta() const;

    void lockMouse();
    void unlockMouse();

private:
    const Uint8* pressedKeyMap;

    std::map<uint32_t, IMouseReceiver*> mouseReceivers;
    std::map<uint32_t, IKeyboardReceiver*> keyboardReceivers;

};
}
