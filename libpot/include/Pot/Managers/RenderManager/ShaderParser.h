#pragma once

#include <Pot/Generic/FiniteStateMachine.h>
#include <Pot/Generic/GenericDefinitions.h>
#include <Pot/Generic/XMLTools.h>

namespace Pot
{

class ShaderParser : public xercesc::DefaultHandler
{
public:

    explicit ShaderParser(std::unordered_map<std::string, GLuint>* shaderPrograms);
    virtual ~ShaderParser();

private:

    // Custom data
    int64_t currProgram;
    int64_t currVertexShader;
    int64_t currFragmentShader;
    std::unordered_map<std::string, GLuint>* programs;

    // FSM data
    std::string strNodeContent;
    typedef Pot::FiniteStateMachine<size_t> FSM;
    FSM fsm;
    FSM::Instance fsmInstance;

    // FSM states
    enum State
    {
        STATE_NOTHING_FOUND,
        STATE_PARSING_ERROR,
        STATE_PARSING_COMPLETED,
        STATE_INSIDE_CONFIG,
        STATE_INSIDE_PROGRAM,
        STATE_INSIDE_SHADER,
        STATE_COUNT
    };

    // FSM events
    enum Event
    {
        EVENT_START_CONFIG,
        EVENT_START_PROGRAM,
        EVENT_START_SHADER,
        EVENT_END_CONFIG,
        EVENT_END_PROGRAM,
        EVENT_END_SHADER,
        EVENT_COUNT
    };

    void startElement(const XMLCh* const uri, const XMLCh* const localname, const XMLCh* const qname, const xercesc::Attributes& attrs);
    void endElement(const XMLCh* const uri, const XMLCh* const localname, const XMLCh* const qname);

    void characters(const XMLCh* const chars, const XMLSize_t length);
};

}
