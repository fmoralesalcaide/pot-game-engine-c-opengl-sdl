#pragma once

#include <Pot/Managers/Manager.h>
#include <Pot/IComponentReceivers/ICollisionReceiver.h>
#include <Pot/Components/Collider.h>
#include <Pot/Managers/CollisionManager/CollisionResult.h>
#include <Pot/Managers/CollisionManager/Octree.h>

namespace Pot
{

class CollisionManager : public Manager
{
public:
    CollisionManager();
    virtual ~CollisionManager();

    virtual void initialize();
    virtual void finish();

    void processCollisions();

    void registerCollider(uint32_t id, Components::Collider* collider);
    void unregisterCollider(uint32_t id);

    void registerCollisionReceiver(uint32_t id, uint32_t gameObjectId, ICollisionReceiver* receiver);
    void unregisterCollisionReceiver(uint32_t id, uint32_t gameObjectId);

    void renderOctree();

private:
    // Colliders
    Octree octree;
    std::map<uint32_t, Components::Collider*> colliders;
    std::unordered_set<uint64_t> currentCollisions;

    // Collision receivers (listeners)
    std::map<uint32_t, ICollisionReceiver*> collisionReceivers;
    std::map<uint32_t, std::set<uint32_t>> collisionReceiversByGameObject;

    uint32_t getGameObjectId(uint32_t componentId);

    void checkColliders(Components::Collider* colA, Components::Collider* colB);
    CollisionResult computeCollision(Components::Collider* colA, Components::Collider* colB);
};

}
