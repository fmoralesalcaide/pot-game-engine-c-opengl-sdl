#pragma once

#include <Pot/Managers/Manager.h>
#include <Pot/Resources/Resource.h>
#include <Pot/Resources/ResourceFactories/MeshResourceFactory.h>

namespace Pot
{

class ResourceManager : public Manager
{
public:
    ResourceManager();
    virtual ~ResourceManager();

    void initialize();
    void finish();

    bool hasResource(Resource::Type type, const std::string& name);
    Resource_sptr loadResource(Resource::Type type, const std::string& name, const std::string& fileName);
    Resource_sptr getResource(Resource::Type type, const std::string& name);

    void removeResource(Resource::Type type, const std::string& name);
    void removeResources(Resource::Type type);

private:
    uint32_t nextResourceId;

    // One hash table and factory per resource type
    MeshResourceFactory meshResourceFactory;
    std::unordered_map<std::string, Resource_sptr> meshResources;

    ResourceFactory* getResourceFactory(Resource::Type type);
    std::unordered_map<std::string, Resource_sptr>& getResourceMap(Resource::Type type);

};

}
