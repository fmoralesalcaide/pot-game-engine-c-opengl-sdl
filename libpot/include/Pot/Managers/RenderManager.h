#pragma once

#include <Pot/IComponentReceivers/IRenderReceiver.h>
#include <Pot/Managers/Manager.h>
#include <Pot/Resources/Resource.h>

namespace Pot
{
class RenderManager : public Manager
{
public:
    RenderManager();
    virtual ~RenderManager();

    void initialize();
    void finish();

    SDL_Window* getSDLWindowHandle() const;
    std::tuple<int,int> getWindowSize() const;

    void render();

    void registerReceiver(uint32_t id, IRenderReceiver* receiver);
    void unregisterReceiver(uint32_t id);

    void loadShaders(const std::string& shadersFile);
    GLuint getShaderDescriptor(const std::string& name = "default") const;

    GLuint appendMesh(const std::vector<GLfloat>& positions, const std::vector<GLfloat>& normals, const std::vector<GLfloat>& uvs, const std::vector<GLuint>& indices, uint32_t numIndices);
    void renderMesh(Resource_sptr mesh);

    // Buffer object getters
    inline GLuint getPositionBuffer() const
    {
        return positionBuffer;
    };
    inline GLuint getNormalBuffer() const
    {
        return normalBuffer;
    };
    inline GLuint getUVBuffer() const
    {
        return uvBuffer;
    };
    inline GLuint getElementBuffer() const
    {
        return elementBuffer;
    };

    void clearBuffers();

private:
    SDL_Window* sdlWindow;
    SDL_GLContext glContext;

    int windowWidth;
    int windowHeight;

    std::map<uint32_t, IRenderReceiver*> receivers;

    // Shaders
    std::unordered_map<std::string, GLuint> shaderPrograms;

    // Meshes
    GLuint positionBuffer;
    GLuint normalBuffer;
    GLuint uvBuffer;
    GLuint elementBuffer;

    uint32_t currIndices;
    uint32_t currPositionBufferOffset;
    uint32_t currNormalBufferOffset;
    uint32_t currUVBufferOffset;
    uint32_t currElementBufferOffset;

    void initializeOpenGL();
    void finishOpengl();

    void resetGeometryBuffers();
};
}
