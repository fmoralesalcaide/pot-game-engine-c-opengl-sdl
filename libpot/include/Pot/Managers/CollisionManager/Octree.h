#pragma once

#include <Pot/Generic/GenericDefinitions.h>
#include <Pot/Components/Collider.h>

/*
 * Axis convention is as follows:
 * i-th children: (x,y,z)
 * 0: (+,+,+)
 * 1: (-,+,+)
 * 2: (+,-,+)
 * 3: (-,-,+)
 * 4: (+,+,-)
 * 5: (-,+,-)
 * 6: (+,-,-)
 * 7: (-,-,-)
 *
 * (note: + includes =)
 *
 * TODO list:
 * - Resize octree root node when out of bound (oob) elements > % total elements.
 * - Efficiently compute collisions between oob elements and inner elements of the octree.
 */

#define OCTREE_DEFAULT_MINRADIUS 4
#define OCTREE_DEFAULT_LB 2
#define OCTREE_DEVAULT_UB 5

namespace Pot
{

class Octree
{
public:

    class Node
    {
    public:
        Node();
        virtual ~Node();

        inline bool isLeaf() const
        {
            return children[0] == nullptr;
        }

        inline unsigned int getNumCollidersLEQ() const { return collidersBelow + colliders.size(); }

        Node* parent;
        Node* children[8];
        glm::vec3 pos;
        unsigned int radius;
        unsigned int collidersBelow;
        std::map<uint32_t, Components::Collider*> colliders;
    };

    Octree(unsigned int minDensity = OCTREE_DEFAULT_LB, unsigned int maxDensity = OCTREE_DEVAULT_UB, bool resizable = true);
    virtual ~Octree();

    void insert(Components::Collider* col, Node* node = nullptr);
    void remove(Components::Collider* col);
    void update(Components::Collider* col);

    void postUpdates();

    std::pair<bool, Node*> getNode(Components::Collider* col);

    void render(Node* node = nullptr);

private:
    unsigned int minDensity;
    unsigned int maxDensity;
    bool resizable;

    Node* root;

    std::unordered_map<uint32_t, Node*> containingNode;
    std::map<uint32_t, Components::Collider*> outliers;

    unsigned int getChildIndex(const glm::vec3& center, const glm::vec3& pos);

    glm::vec3 getChildDirection(unsigned int i);

    void checkExpand(Node* node);
    void checkContract(Node* node);
    void checkOutliers();
    void checkRoot();

    bool isWithinOctet(Components::Collider* col, Octree::Node* node);
    glm::vec3 computeCentroid(const std::map<uint32_t, Components::Collider*>& colliders);

};

}
