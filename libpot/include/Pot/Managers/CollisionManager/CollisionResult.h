#pragma once

namespace Pot
{

class CollisionResult
{
public:
    CollisionResult();
    virtual ~CollisionResult();

    bool isColliding() const;
    void setColliding(bool collide);

private:
    bool colliding;
};

}
