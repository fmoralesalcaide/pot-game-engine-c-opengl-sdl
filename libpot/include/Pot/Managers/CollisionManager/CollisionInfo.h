#pragma once

#include <Pot/Generic/GenericDefinitions.h>
#include <Pot/GameObject.h>
#include <Pot/Components/Collider.h>

namespace Pot
{

class CollisionInfo
{
public:
    CollisionInfo(Components::Collider* collidingCollider);
    virtual ~CollisionInfo();

    Components::Collider* getCollidingCollider() const;

private:
    Components::Collider* collidingCollider;
};

}
