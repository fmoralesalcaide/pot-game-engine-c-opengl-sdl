#pragma once

#include <sstream>
#include <stdexcept>
#include <string>

#define LOCATE_EXECUTION(oss) oss << __FILE__ << ":" << __LINE__;

#define THROW_EXCEPTION(message)				\
do{												\
	std::ostringstream oss;						\
	LOCATE_EXECUTION(oss)						\
	oss << ": " << message;						\
	oss << std::endl;							\
	throw std::runtime_error(oss.str());		\
}while(0)
