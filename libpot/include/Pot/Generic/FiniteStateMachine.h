#pragma once

#include <Pot/Generic/GenericDefinitions.h>

namespace Pot
{

template<typename T>
class FiniteStateMachine
{
public:

    class Instance
    {
    public:

        explicit Instance(const FiniteStateMachine* fsm)
            : fsm(fsm)
        {
            if(!fsm)
                THROW_EXCEPTION("Null pointer Finite State Machine received.");

            if(!fsm->getNumStates())
                THROW_EXCEPTION("No states were defined for the Finite State Machine.");

            currentState = fsm->getInitialState();
            stateData = new T[fsm->getNumStates()];

            for(std::size_t state = 0; state < fsm->getNumStates(); state++)
                stateData[state] = fsm->getDefaultStateData();
        }

        virtual ~Instance()
        {
            delete[] stateData;
        }

        /*!
            Returns the current state of the fsm instance.
        */
        std::size_t getCurrentState()
        {
            return currentState;
        }

        /*!
            Re-sets the fsm instance to the default initial state.
        */
        void reset()
        {
            currentState = fsm->getInitialState();

            for(std::size_t state = 0; state < fsm->getNumStates(); state++)
                stateData[state] = fsm->getDefaultStateData();
        }

        /*!
            Signals an event to the fsm instance, triggering a switch
            to the next state.
        */
        void signalEvent(std::size_t event)
        {
            if(event >= fsm->getNumEvents())
                THROW_EXCEPTION("Event " << event << " does not exist");

            currentState = fsm->getNextState(currentState, event);
        }

        /*!
            Signals an event to the fsm instance, triggering a switch
            to the next state.
        */
        T getStateData(std::size_t state)
        {
            if(state >= fsm->getNumStates())
                THROW_EXCEPTION("State " << state << " does not exist");

            return stateData[state];
        }

        /*!
            Sets the data attached to a state.
        */
        void setStateData(std::size_t state, T newStateData)
        {
            if(state >= fsm->getNumStates())
                THROW_EXCEPTION("State " << state << " does not exist");

            stateData[state] = newStateData;
        }

    private:
        //! The FiniteStateMachine providing the abstracted logic of the fsm (states and transitions).
        const FiniteStateMachine* fsm;

        //! The current state of the fsm instance.
        std::size_t currentState;

        //! A pointer to an array of templated data objects, that are mapped in order to the states of the fsm.
        T* stateData;
    };

    /*!
        Initializes a finite state machine, by setting the number of states, events, the initial and error states, and a default state data of template type T.
        Checks are performed during construction of a FiniteStateMachine in order to ensure the correct structure of the fsm. In addition, the transition
        matrix of the fsm is initialized empty.
    */
    explicit FiniteStateMachine(std::size_t numStates, std::size_t numEvents, std::size_t initialState, std::size_t errorState, T defaultStateData)
        : numStates(numStates), numEvents(numEvents), initialState(initialState), errorState(errorState), defaultStateData(defaultStateData)
    {
        if(numStates < 2)
            THROW_EXCEPTION("Insufficient number of states.");

        if(numEvents < 1)
            THROW_EXCEPTION("Insufficient number of events.");

        if(initialState >= numStates)
            THROW_EXCEPTION("Initial state does not exist.");

        if(errorState >= numStates)
            THROW_EXCEPTION("Error state does not exist.");

        transitionMatrix = new std::size_t*[numStates];

        for(std::size_t state = 0; state < numStates; state++)
        {
            transitionMatrix[state] = new std::size_t[numEvents];

            for(std::size_t event = 0; event < numEvents; event++)
                transitionMatrix[state][event] = errorState;
        }
    }

    explicit FiniteStateMachine(const FiniteStateMachine<T>& fsm)
        : numStates(fsm.numStates), numEvents(fsm.numEvents), initialState(fsm.initialState), errorState(fsm.errorState), defaultStateData(fsm.defaultStateData)
    {
        transitionMatrix = new std::size_t*[fsm.numStates];

        for(std::size_t state = 0; state < fsm.numStates; state++)
        {
            transitionMatrix[state] = new std::size_t[fsm.numEvents];

            for(std::size_t event = 0; event < fsm.numEvents; event++)
                transitionMatrix[state][event] = fsm.transitionMatrix[state][event];
        }
    }

    virtual ~FiniteStateMachine()
    {
        for(std::size_t state = 0; state < numStates; state++)
            delete[] transitionMatrix[state];

        delete[] transitionMatrix;
    }

    std::size_t getNumStates() const
    {
        return numStates;
    }

    std::size_t getNumEvents() const
    {
        return numEvents;
    }

    std::size_t getInitialState() const
    {
        return initialState;
    }

    std::size_t getErrorState() const
    {
        return errorState;
    }

    T getDefaultStateData() const
    {
        return defaultStateData;
    }

    /*!
        Adds a new transition into the FiniteStateMachine transition matrix.
    */
    void addTransition(std::size_t previousState, std::size_t event, std::size_t newState)
    {
        if(previousState >= numStates)
            THROW_EXCEPTION("Previous state does not exist.");

        if(event >= numEvents)
            THROW_EXCEPTION("Event does not exist.");

        if(newState >= numStates)
            THROW_EXCEPTION("New state does not exist.");

        transitionMatrix[previousState][event] = newState;
    }

    /*!
        Returns the state obtained by applying the given event in the given state.
    */
    std::size_t getNextState(std::size_t previousState, std::size_t event) const
    {
        if(previousState >= numStates)
            THROW_EXCEPTION("Previous state " << previousState << " does not exist");

        if(event >= numEvents)
            THROW_EXCEPTION("Event " << event << " does not exist");

        return transitionMatrix[previousState][event];
    }

private:

    //! Number of states of the fsm.
    std::size_t numStates;

    //! Number of events of the fsm.
    std::size_t numEvents;

    //! Transition matrix of the fsm.
    std::size_t** transitionMatrix;

    //! Initial state of the fsm.
    std::size_t initialState;

    //! Error state of the fsm. An error state is included by default to handled unexpected transitions during the execution of a fsm instance.
    std::size_t errorState;

    //! Default state data that is mapped to all the states in the absence of state-specific data during the initialization of a FiniteStateMachine::Instance.
    T defaultStateData;
};
}
