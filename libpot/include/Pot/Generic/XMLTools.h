#pragma once

#include <Pot/Generic/GenericDefinitions.h>
#include <Pot/Generic/StringTools.h>

#include <xercesc/sax/InputSource.hpp>
#include <xercesc/sax2/Attributes.hpp>
#include <xercesc/sax2/DefaultHandler.hpp>
#include <xercesc/sax2/XMLReaderFactory.hpp>
#include <xercesc/framework/MemBufInputSource.hpp>
#include <xercesc/framework/LocalFileInputSource.hpp>
#include <xercesc/framework/URLInputSource.hpp>
#include <xercesc/util/OutOfMemoryException.hpp>
#include <xercesc/util/XMLURL.hpp>


namespace Pot
{
namespace XMLTools
{
class XMLTranscoders
{
public:

    static std::string transcodeXMLChars2String(const XMLCh* const chars)
    {
        char* cChars = xercesc::XMLString::transcode(chars);

        if(!cChars)
            THROW_EXCEPTION("Transcode failed.");

        std::string s(cChars);
        xercesc::XMLString::release(&cChars);
        return s;
    }

    static std::string transcodeXMLChars2String(const XMLCh* const chars, const XMLSize_t length)
    {
        char* cChars = xercesc::XMLString::transcode(chars);

        if(!cChars)
            THROW_EXCEPTION("Transcode failed.");

        std::string s(cChars, length);
        xercesc::XMLString::release(&cChars);
        return s;
    }

    static XMLCh*
    transcodeString2XMLChars(const char* const chars)
    {
        XMLCh* xmlChars = xercesc::XMLString::transcode(chars);

        if(!xmlChars)
            THROW_EXCEPTION("Transcode failed.");

        return xmlChars;
    }

    static XMLCh*
    transcodeString2XMLChars(const std::string& chars)
    {
        const char* const data = chars.c_str();
        XMLCh* xmlChars = xercesc::XMLString::transcode(data);

        if(!xmlChars)
            THROW_EXCEPTION("Transcode failed.");

        return xmlChars;
    }

    static void releaseChs(char* chars)
    {
        xercesc::XMLString::release(&chars);
    }

    static void releaseXMLChs(XMLCh* chars)
    {
        xercesc::XMLString::release(&chars);
    }

    static void releaseXMLChs(const XMLCh* const chars)
    {
        XMLCh* chars2 = (XMLCh*) chars;
        xercesc::XMLString::release(&chars2);
    }
};

class XMLAttributesReader
{
public:

    static bool existsAttribute(const xercesc::Attributes& attrs, const std::string& attrName)
    {
        XMLCh* xmlchName = XMLTranscoders::transcodeString2XMLChars(attrName.c_str());
        int index = attrs.getIndex(xmlchName);
        XMLTranscoders::releaseXMLChs(xmlchName);
        return index != -1;
    }

    static std::size_t getNumAttributes(const xercesc::Attributes& attrs)
    {
        return attrs.getLength();
    }

    static std::pair<std::string, std::string> getAttribute(const xercesc::Attributes& attrs, std::size_t index)
    {
        if(index >= attrs.getLength())
            THROW_EXCEPTION("Attribute index (" << index << ") out of range [0.." << attrs.getLength() << "]");

        const XMLCh* const qname = attrs.getQName(index);
        std::string strName(XMLTranscoders::transcodeXMLChars2String(qname));
        const XMLCh* const value = attrs.getValue(index);
        std::string strValue(XMLTranscoders::transcodeXMLChars2String(value));
        return std::make_pair(strName, strValue);
    }

    template<typename T>
    static T getAttributeValue(const xercesc::Attributes& attrs, const std::string& attrName)
    {
        XMLCh* xmlchName = XMLTranscoders::transcodeString2XMLChars(attrName.c_str());
        const XMLCh* xmlchValue = attrs.getValue(xmlchName);
        XMLTranscoders::releaseXMLChs(xmlchName);

        if(!xmlchValue)
            THROW_EXCEPTION("Attribute '" << attrName << "' does not exist");

        std::string strAttrValue = XMLTranscoders::transcodeXMLChars2String(xmlchValue);
        T attrValue = Pot::StringTools::parse<T>(strAttrValue);
        return attrValue;
    }

    template<typename T>
    static T getAttributeValueEnum(const xercesc::Attributes& attrs, const std::string& attrName, const std::map<std::string, T>& valuePairs)
    {
        XMLCh* xmlchName = XMLTranscoders::transcodeString2XMLChars(attrName.c_str());
        const XMLCh* xmlchValue = attrs.getValue(xmlchName);
        XMLTranscoders::releaseXMLChs(xmlchName);

        if(!xmlchValue)
            THROW_EXCEPTION("Attribute '" << attrName << "' does not exist");

        std::string strAttrValue = XMLTranscoders::transcodeXMLChars2String(xmlchValue);
        typename std::map<std::string, T>::const_iterator it = valuePairs.find(strAttrValue);

        if(it == valuePairs.end())
        {
            std::string strTypeName = Pot::StringTools::typeName(typeid(T));
            THROW_EXCEPTION("Unable to parse '" << strAttrValue << "' as '" << strTypeName << "'");
        }

        T attrValue = (*it).second;
        return (attrValue);
    }
};

class XMLParser
{
private:

    xercesc::SAX2XMLReader* parser;

public:

    enum InputKind
    {
        INPUTKIND_LOCALFILE, INPUTKIND_XMLSTRING, INPUTKIND_URLPATH, INPUTKIND_RESTAPI
    };

    explicit XMLParser()
        : parser(nullptr)
    {
        // Initialize Xerces
        xercesc::XMLPlatformUtils::Initialize();
        // Create a SAX parser object.
        parser = xercesc::XMLReaderFactory::createXMLReader();
        parser->setFeature(xercesc::XMLUni::fgSAX2CoreValidation, true);
        parser->setFeature(xercesc::XMLUni::fgXercesDynamic, true);
        parser->setFeature(xercesc::XMLUni::fgSAX2CoreNameSpaces, true);
        parser->setFeature(xercesc::XMLUni::fgXercesSchema, true);
        parser->setFeature(xercesc::XMLUni::fgXercesHandleMultipleImports, true);
        parser->setFeature(xercesc::XMLUni::fgXercesSchemaFullChecking, true);
        parser->setFeature(xercesc::XMLUni::fgSAX2CoreNameSpacePrefixes, true);
    }

    virtual ~XMLParser()
    {
        // Delete the Parser
        if(parser != nullptr)
        {
            delete parser;
            parser = nullptr;
        }

        // Terminate Xerces
        xercesc::XMLPlatformUtils::Terminate();
    }

    template<class DataContainer, class Handler>
    void parseData(DataContainer* parsedData, const std::string& inputData, InputKind inputKind = INPUTKIND_LOCALFILE)
    {
        xercesc::InputSource* inputSource = nullptr;
        XMLCh* xmlStrPath = nullptr;
        char* xmlChars = nullptr;
        xercesc::XMLURL* xmlURL = nullptr;

        try
        {
            // Create the input source
            switch(inputKind)
            {
            case INPUTKIND_LOCALFILE:
                if(!inputData.length())
                    throw std::runtime_error("Empty file path received.");

                xmlStrPath = XMLTranscoders::transcodeString2XMLChars(inputData);
                inputSource = new xercesc::LocalFileInputSource(xmlStrPath);
                break;

            case INPUTKIND_XMLSTRING:
                if(!inputData.length())
                    throw std::runtime_error("Empty XML string received.");

                xmlChars = (char*) inputData.c_str();
                inputSource = new xercesc::MemBufInputSource((const XMLByte*) xmlChars, strlen(xmlChars), "XMLParser", false);
                break;

            case INPUTKIND_URLPATH:
                if(!inputData.length())
                    THROW_EXCEPTION("Empty URL received.");

                xmlStrPath = XMLTranscoders::transcodeString2XMLChars(inputData);
                xmlURL = new xercesc::XMLURL(xmlStrPath);
                inputSource = new xercesc::URLInputSource(*xmlURL);
                break;

            case INPUTKIND_RESTAPI:
                THROW_EXCEPTION("UNSUPPORTED INPUT KIND.");
                break;
            }

            // Create the handler object and install it as the document and error handler for the parser.
            // Then parse the file and catch exceptions thrown.
            Handler handler(parsedData);
            parser->setContentHandler(&handler);
            parser->setErrorHandler(&handler);
            parser->parse(*inputSource);
            int errorCount = parser->getErrorCount();

            if(errorCount > 0)
                THROW_EXCEPTION("Unsuccessful parsing.");
        }
        catch(const xercesc::XMLException& e)
        {
            if(inputSource)
                delete inputSource;

            if(xmlURL)
                delete xmlURL;

            if(xmlStrPath)
                XMLTranscoders::releaseXMLChs(xmlStrPath);

            std::string strError = XMLTranscoders::transcodeXMLChars2String(e.getMessage());
            THROW_EXCEPTION("Caught xercesc::XMLException while parsing XML data: " << strError);
        }
        catch(const xercesc::SAXException& e)
        {
            if(inputSource)
                delete inputSource;

            if(xmlURL)
                delete xmlURL;

            if(xmlStrPath)
                XMLTranscoders::releaseXMLChs(xmlStrPath);

            std::string strError = XMLTranscoders::transcodeXMLChars2String(e.getMessage());
            THROW_EXCEPTION("Caught xercesc::XMLException while parsing XML data: " << strError);
        }
        catch(const xercesc::OutOfMemoryException& e)
        {
            if(inputSource)
                delete inputSource;

            if(xmlURL)
                delete xmlURL;

            if(xmlStrPath)
                XMLTranscoders::releaseXMLChs(xmlStrPath);

            std::string strError = XMLTranscoders::transcodeXMLChars2String(e.getMessage());
            THROW_EXCEPTION("Caught xercesc::XMLException while parsing XML data: " << strError);
        }
        catch(std::runtime_error& e)
        {
            if(inputSource)
                delete inputSource;

            if(xmlURL)
                delete xmlURL;

            if(xmlStrPath)
                XMLTranscoders::releaseXMLChs(xmlStrPath);

            THROW_EXCEPTION("Caught xercesc::XMLException while parsing XML data: " << e.what());
        }

        if(inputSource)
            delete inputSource;

        if(xmlURL)
            delete xmlURL;

        if(xmlStrPath)
            XMLTranscoders::releaseXMLChs(xmlStrPath);
    }
};
}
}
