#pragma once

#include <Pot/Generic/GenericDefinitions.h>

namespace Pot
{
namespace StringToolsParsers
{
template<class T>
class parse : public std::binary_function<std::string, T, bool>
{
public:

    bool operator()(const std::string& srcValue, T& dstValue) const;
};
}

class StringTools
{
public:

    static std::string toLowerCase(const std::string& originalString)
    {
        std::string newString;
        newString.resize(originalString.length());
        std::transform(originalString.begin(), originalString.end(), newString.begin(), ::tolower);
        return (newString);
    }

    static std::string toUpperCase(const std::string& originalString)
    {
        std::string newString;
        newString.resize(originalString.length());
        std::transform(originalString.begin(), originalString.end(), newString.begin(), ::toupper);
        return (newString);
    }

    static std::string replaceFirst(const std::string& originalString, const std::string& strToBeReplaced, const std::string& strReplacer)
    {
        std::string newString(originalString);
        size_t position = newString.find(strToBeReplaced, 0);

        if(position != std::string::npos)
        {
            size_t len = strToBeReplaced.length();
            newString.replace(position, len, strReplacer);
        }

        return (newString);
    }

    static std::string replaceAll(const std::string& originalString, const std::string& strToBeReplaced, const std::string& strReplacer)
    {
        std::string newString(originalString);
        size_t position = 0;
        size_t len = strToBeReplaced.length();
        size_t len2 = strReplacer.length();

        while(position != std::string::npos)
        {
            position = newString.find(strToBeReplaced, position);

            if(position != std::string::npos)
            {
                newString.replace(position, len, strReplacer);
                position += len2;
            }
        }

        return (newString);
    }

    static std::vector<std::string> split(const std::string& originalString, char delimiter, bool keepEmptyTokens)
    {
        std::vector<std::string> elements;
        std::stringstream stream(originalString);
        std::string token;

        while(std::getline(stream, token, delimiter))
            if((token.length() > 0) || keepEmptyTokens)
                elements.push_back(token);

        return (elements);
    }

    static std::string typeName(const std::type_info& type)
    {
        // Demangle GCC type name returned by typeid::name
        const char* const name = type.name();
        int status = -4;
        char* res = abi::__cxa_demangle(name, 0, 0, &status);
        const char* const cDemangledName = ((status == 0) ? res : name);
        std::string stdTypeName(cDemangledName);
        free(res);
        return (stdTypeName);
    }

    template<typename T>
    static T parse(const std::string strValue)
    {
        T value;
        bool res = Pot::StringToolsParsers::parse<T>()(strValue, value);

        if(!res)
        {
            std::string strTypeName = typeName(typeid(T));
            std::ostringstream ss;
            ss << "Unable to parse '" << strValue << "' as '" << strTypeName << "'";
            throw std::runtime_error(ss.str());
        }

        return (value);
    }

    static std::string format(const std::string& fmt, ...)
    {
        std::string res = "";
        va_list args;
        va_start(args, fmt);
        std::size_t pos = 0;
        std::size_t len = fmt.length();

        while(pos < len)
        {
            std::size_t tagBeginPos = fmt.find('{', pos);

            if(tagBeginPos == std::string::npos)
            {
                // no occurrence
                res += fmt.substr(pos, tagBeginPos - pos);
                pos += (tagBeginPos - pos);
            }
            else
            {
                // occurrence
                // copy text before tag
                res += fmt.substr(pos, tagBeginPos - pos);
                pos += (tagBeginPos - pos);

                if((tagBeginPos + 1) >= len)
                {
                    va_end(args);
                    throw std::runtime_error("Tag opened but not closed.");
                }

                if(fmt.at(tagBeginPos + 1) == '{')
                {
                    // not a tag beginner, but a specific '{' character
                    res += '{';
                    pos += 2;
                }
                else
                {
                    // inside a tag
                    std::size_t tagEndPos = fmt.find('}', tagBeginPos + 1);

                    if(tagEndPos == std::string::npos)
                    {
                        va_end(args);
                        throw std::runtime_error("Tag opened but not closed.");
                    }

                    if(tagEndPos == (tagBeginPos + 1))
                    {
                        va_end(args);
                        throw std::runtime_error("Empty tag found.");
                    }

                    std::string tag = fmt.substr(tagBeginPos + 1, tagEndPos - tagBeginPos - 1);
                    pos += (tagEndPos - tagBeginPos + 1);

                    if(tag.find('*') < std::string::npos)
                    {
                        va_end(args);
                        throw std::runtime_error("Variable length tags not supported.");
                    }

                    std::size_t tagLen = tag.length();
                    char tagSpec = tag.at(tagLen - 1);

                    switch(tagSpec)
                    {
                    case 'd':
                    case 'i':
                    {
                        char tagLen2 = '\0';

                        if(tagLen > 1)
                            tagLen2 = tag.at(tagLen - 2);

                        switch(tagLen2)
                        {
                        case 'h':
                        {
                            // warning: ‘short int’/‘signed char’ is promoted to ‘int’ when passed through ‘...’ [enabled by default]
                            // note: (so you should pass ‘int’ not ‘short int’/‘signed char’ to ‘va_arg’)
                            // note: if this code is reached, the program will abort
                            char tagLen1 = '\0';

                            if(tagLen > 2)
                                tagLen1 = tag.at(tagLen - 3);

                            if(tagLen1 == 'h')
                                res += getFormattedAttribute<int /*signed char*/>(args, tag, va_arg(args, int /*signed char*/));
                            else
                                res += getFormattedAttribute<int /*short int*/>(args, tag, va_arg(args, int /*short int*/));

                            break;
                        }

                        case 'l':
                        {
                            // warning: ISO C++ 1998 does not support ‘long long’ [-Wlong-long]
                            // char tagLen1 = '\0';
                            // if(tagLen > 2)
                            //   tagLen1 = tag.at(tagLen - 3);
                            // if(tagLen1 == 'l')
                            //   res += getFormattedAttribute<long long int>(args, tag);
                            // else
                            res += getFormattedAttribute<long int>(args, tag, va_arg(args, long int));
                            break;
                        }

                        case 'j':
                            res += getFormattedAttribute<intmax_t>(args, tag, va_arg(args, intmax_t));
                            break;

                        case 'z':
                            res += getFormattedAttribute<size_t>(args, tag, va_arg(args, size_t));
                            break;

                        case 't':
                            res += getFormattedAttribute<ptrdiff_t>(args, tag, va_arg(args, ptrdiff_t));
                            break;

                        default:
                            res += getFormattedAttribute<int>(args, tag, va_arg(args, int));
                            break;
                        }

                        break;
                    }

                    case 'u':
                    case 'o':
                    case 'x':
                    case 'X':
                    {
                        char tagLen2 = '\0';

                        if(tagLen > 1)
                            tagLen2 = tag.at(tagLen - 2);

                        switch(tagLen2)
                        {
                        case 'h':
                        {
                            // warning: ‘short unsigned int’/‘unsigned char’ is promoted to ‘int’ when passed through ‘...’ [enabled by default]
                            // note: (so you should pass ‘int’ not ‘short unsigned int’/‘unsigned char’ to ‘va_arg’)
                            // note: if this code is reached, the program will abort
                            char tagLen1 = '\0';

                            if(tagLen > 2)
                                tagLen1 = tag.at(tagLen - 3);

                            if(tagLen1 == 'h')
                                res += getFormattedAttribute<int /*unsigned char*/>(args, tag, va_arg(args, int /*unsigned char*/));
                            else
                                res += getFormattedAttribute<int /*unsigned short int*/>(args, tag, va_arg(args, int /*unsigned short int*/));

                            break;
                        }

                        case 'l':
                        {
                            // warning: ISO C++ 1998 does not support ‘long long’ [-Wlong-long]
                            // char tagLen1 = '\0';
                            // if(tagLen > 2)
                            //   tagLen1 = tag.at(tagLen - 3);
                            // if(tagLen1 == 'l')
                            //   res += getFormattedAttribute<unsigned long long int>(args, tag);
                            // else
                            res += getFormattedAttribute<unsigned long int>(args, tag, va_arg(args, unsigned long int));
                            break;
                        }

                        case 'j':
                            res += getFormattedAttribute<uintmax_t>(args, tag, va_arg(args, uintmax_t));
                            break;

                        case 'z':
                            res += getFormattedAttribute<size_t>(args, tag, va_arg(args, size_t));
                            break;

                        case 't':
                            res += getFormattedAttribute<ptrdiff_t>(args, tag, va_arg(args, ptrdiff_t));
                            break;

                        default:
                            res += getFormattedAttribute<unsigned int>(args, tag, va_arg(args, unsigned int));
                            break;
                        }

                        break;
                    }

                    case 'f':
                    case 'F':
                    case 'e':
                    case 'E':
                    case 'g':
                    case 'G':
                    case 'a':
                    case 'A':
                    {
                        char tagLen2 = '\0';

                        if(tagLen > 1)
                            tagLen2 = tag.at(tagLen - 2);

#if (defined __x86_64__)

                        switch(tagLen2)
                        {
                        case 'L':
                            res += getFormattedAttribute<long double>(args, tag, va_arg(args, double_t));
                            break;

                        default:
                            res += getFormattedAttribute<double>(args, tag, va_arg(args, double));
                            break;
                        }

#else

                        switch(tagLen2)
                        {
                        case 'L':
                            res += getFormattedAttribute<double_t> (args, tag, va_arg (args, double_t) );
                            break;

                        default:
                            res += getFormattedAttribute<float_t> (args, tag, va_arg (args, float_t) );
                            break;
                        }

#endif
                        break;
                    }

                    case 'c':
                    {
                        char tagLen2 = '\0';

                        if(tagLen > 1)
                            tagLen2 = tag.at(tagLen - 2);

                        switch(tagLen2)
                        {
                        case 'l':
                            res += getFormattedAttribute<wint_t>(args, tag, va_arg(args, wint_t));
                            break;

                        default:
                            res += getFormattedAttribute<int>(args, tag, va_arg(args, int));
                            break;
                        }

                        break;
                    }

                    case 's':
                    {
                        char tagLen2 = '\0';

                        if(tagLen > 1)
                            tagLen2 = tag.at(tagLen - 2);

                        switch(tagLen2)
                        {
                        case 'l':
                            res += getFormattedAttribute(args, tag, va_arg(args, wchar_t*));
                            break;

                        default:
                            res += getFormattedAttribute(args, tag, va_arg(args, char*));
                            break;
                        }

                        break;
                    }

                    case 'p':
                    {
                        res += getFormattedAttribute<void*>(args, tag, va_arg(args, void*));
                        break;
                    }

                    default:
                    {
                        va_end(args);
                        std::ostringstream ss;
                        ss << "Tag '" << tag << "' not supported.";
                        throw std::runtime_error(ss.str());
                    }
                    }
                }
            }
        }

        va_end(args);
        return (res);
    }

    static std::string dateTime()
    {
        time_t rawtime = time(nullptr);

        if(rawtime == -1)
            throw std::runtime_error("Unable to obtain calendar time.");

        struct tm* timeinfo = localtime(&rawtime);
        std::string sDateTime = format("{04d}/{02d}/{02d} {02d}:{02d}:{02d}", timeinfo->tm_year + 1900, timeinfo->tm_mon + 1, timeinfo->tm_mday, timeinfo->tm_hour, timeinfo->tm_min, timeinfo->tm_sec);
        return (sDateTime);
    }

private:

// used by stringFormat()
    template<typename T>
    static std::string getFormattedAttribute(va_list args, std::string tag, T value)
    {
        char buffer[4096];
        tag = '%' + tag;
        int n = sprintf(buffer, tag.c_str(), value);

        if(n < 0)
        {
            va_end(args);
            throw std::runtime_error("Error formatting tag '" + tag + "'");
        }

        return (std::string(buffer));
    }

    static std::string getFormattedAttribute(va_list args, std::string tag, char* value)
    {
        char* buffer = (char*) malloc((strlen(value) + 1) * sizeof(char));
        tag = '%' + tag;
        int n = sprintf(buffer, tag.c_str(), value);

        if(n < 0)
        {
            va_end(args);
            std::ostringstream ss;
            ss << "Error formatting tag '" << tag << "'.";
            throw std::runtime_error(ss.str());
        }

        std::string s(buffer);
        free(buffer);
        return (s);
    }

    static std::string getFormattedAttribute(va_list args, std::string tag, wchar_t* value)
    {
        char* buffer = (char*) malloc((wcslen(value) + 1) * sizeof(wchar_t));
        tag = '%' + tag;
        int n = sprintf(buffer, tag.c_str(), value);

        if(n < 0)
        {
            va_end(args);
            std::ostringstream ss;
            ss << "Error formatting tag '" << tag << "'.";
            throw std::runtime_error(ss.str());
        }

        std::string s(buffer);
        free(buffer);
        return (s);
    }

};

namespace StringToolsParsers
{

template<>
class parse<std::string> : std::binary_function<std::string, std::string, bool>
{
public:

    bool operator()(const std::string& srcValue, std::string& dstValue) const
    {
        dstValue = srcValue;
        return (true);
    }
};

template<>
class parse<bool> : std::binary_function<std::string, bool, bool>
{
public:

    bool operator()(const std::string& srcValue, bool& dstValue) const
    {
        if(Pot::StringTools::toUpperCase(srcValue).compare("TRUE") == 0)
        {
            dstValue = true;
            return (true);
        }
        else if(Pot::StringTools::toUpperCase(srcValue).compare("FALSE") == 0)
        {
            dstValue = false;
            return (true);
        }
        else
            return (false);
    }
};

template<>
class parse<uint8_t> : std::binary_function<std::string, uint8_t, bool>
{
public:

    bool operator()(const std::string& srcValue, uint8_t& dstValue) const
    {
        uint16_t x;
        int res = sscanf(srcValue.c_str(), "%hu", &x);

        if(res != 1)
            return (false);

        if(x > UCHAR_MAX)
            return (false);
        else
            dstValue = x;

        return (true);
    }
};

template<>
class parse<uint16_t> : std::binary_function<std::string, uint16_t, bool>
{
public:

    bool operator()(const std::string& srcValue, uint16_t& dstValue) const
    {
        int res = sscanf(srcValue.c_str(), "%hu", &dstValue);
        return (res == 1);
    }
};

template<>
class parse<size_t> : std::binary_function<std::string, size_t, bool>
{
public:

    bool operator()(const std::string& srcValue, size_t& dstValue) const
    {
        int res = sscanf(srcValue.c_str(), "%zu", &dstValue);
        return (res == 1);
    }
};

template<>
class parse<int8_t> : std::binary_function<std::string, int8_t, bool>
{
public:

    bool operator()(const std::string& srcValue, int8_t& dstValue) const
    {
        int16_t x;
        int res = sscanf(srcValue.c_str(), "%hd", &x);

        if(res != 1)
            return (false);

        if(x > SCHAR_MAX)
            return (false);

        if(x < SCHAR_MIN)
            return (false);

        dstValue = x;
        return (true);
    }
};

template<>
class parse<int16_t> : std::binary_function<std::string, int16_t, bool>
{
public:

    bool operator()(const std::string& srcValue, int16_t& dstValue) const
    {
        int res = sscanf(srcValue.c_str(), "%hd", &dstValue);
        return (res == 1);
    }
};

template<>
class parse<int32_t> : std::binary_function<std::string, int32_t, bool>
{
public:

    bool operator()(const std::string& srcValue, int32_t& dstValue) const
    {
        int res = sscanf(srcValue.c_str(), "%d", &dstValue);
        return (res == 1);
    }
};

template<>
class parse<int64_t> : std::binary_function<std::string, int64_t, bool>
{
public:

    bool operator()(const std::string& srcValue, int64_t& dstValue) const
    {
        int res = sscanf(srcValue.c_str(), "%ld", &dstValue);
        return (res == 1);
    }
};

#if (defined __x86_64__)
template<>
class parse<float_t> : std::binary_function<std::string, float_t, bool>
{
public:

    bool operator()(const std::string& srcValue, float_t& dstValue) const
    {
        int res = sscanf(srcValue.c_str(), "%f", &dstValue);
        return (res == 1);
    }
};

template<>
class parse<double_t> : std::binary_function<std::string, double_t, bool>
{
public:

    bool operator()(const std::string& srcValue, double_t& dstValue) const
    {
        int res = sscanf(srcValue.c_str(), "%lf", &dstValue);
        return (res == 1);
    }
};
#else
template<>
class parse<float_t> : std::binary_function<std::string, float_t, bool>
{
public:

    bool operator() (const std::string& srcValue, float_t& dstValue) const
    {
        int res = sscanf (srcValue.c_str(), "%Lf", & dstValue);
        return (res == 1);
    }
};
#endif

}

}
