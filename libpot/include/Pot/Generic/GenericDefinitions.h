#pragma once

// STL
#include <set>
#include <map>
#include <list>
#include <tuple>
#include <vector>
#include <unordered_map>
#include <unordered_set>

#include <cmath>
#include <chrono>
#include <climits>
#include <memory>
#include <algorithm>

#include <cstdio>
#include <cstring>
#include <cwchar>
#include <string>
#include <fstream>
#include <sstream>
#include <iostream>

#include <cstdarg>
#include <cstddef>
#include <cxxabi.h>
#include <stdint.h>
#include <stdexcept>
#include <typeinfo>
#include <type_traits>

// Boost
#include <boost/noncopyable.hpp>

// SDL2
#include <SDL2/SDL.h>

// OpenGL
#include <GL/glew.h>
#include <GL/gl.h>

// OpenGL data structures
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/random.hpp>
#include <glm/gtx/quaternion.hpp>
#include <glm/gtx/component_wise.hpp>

#define BUFFER_OFFSET(idx) (static_cast<char*>(0) + (idx))

// Exception
#include <Pot/Generic/Exception.h>
