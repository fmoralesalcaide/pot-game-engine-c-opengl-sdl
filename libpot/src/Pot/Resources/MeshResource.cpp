#include <Pot/Resources/MeshResource.h>

namespace Pot
{

MeshResource::MeshResource(
    uint32_t id,
    const std::string& name,
    const std::vector<GLfloat>& positions,
    const std::vector<GLfloat>& normals,
    const std::vector<GLfloat>& uvs,
    const std::vector<GLuint>& indices,
    GLuint offsetInBuffer
)
    : Resource(id, Resource::Type::MESH, name),
      positions(positions),
      normals(normals),
      uvs(uvs),
      indices(indices),
      offsetInBuffer(offsetInBuffer)
{
}

MeshResource::~MeshResource()
{
}

const std::vector<GLfloat>& MeshResource::getPositions() const
{
    return positions;
}

const std::vector<GLfloat>& MeshResource::getNormals() const
{
    return normals;
}

const std::vector<GLfloat>& MeshResource::getUVs() const
{
    return uvs;
}

const std::vector<GLuint>& MeshResource::getIndices() const
{
    return indices;
}

GLuint MeshResource::getOffsetInBuffer() const
{
    return offsetInBuffer;
}

}
