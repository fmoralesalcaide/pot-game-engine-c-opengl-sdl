#include <Pot/Resources/ResourceFactories/MeshResourceFactory.h>
#include <Pot/Generic/TinyObjLoader.h>
#include <Pot/Managers/RenderManager.h>
#include <Pot/Engine.h>
#include <Pot/Resources/MeshResource.h>

namespace Pot
{

MeshResourceFactory::MeshResourceFactory()
    : ResourceFactory(Resource::Type::MESH)
{
}

MeshResourceFactory::~MeshResourceFactory()
{
}

Resource_sptr MeshResourceFactory::loadResource(uint32_t id, const std::string& name, const std::string& fileName)
{
    // Parse OBJ file
    tinyobj::attrib_t attributes;
    std::vector<tinyobj::shape_t> vShapes;
    std::vector<tinyobj::material_t> vMaterials;
    std::string err;

    std::ifstream objFileStream(fileName);
    if(!objFileStream.is_open())
        THROW_EXCEPTION("Could not open .obj file " << fileName << " for parsing");

    bool ret = tinyobj::LoadObj(&attributes, &vShapes, &vMaterials, &err, &objFileStream, nullptr, true);

    objFileStream.close();

    if(!err.empty())
        THROW_EXCEPTION("Could not load .obj mesh " << fileName << ": " << err);
    if(!ret)
        THROW_EXCEPTION("Could not load .obj mesh " << fileName << ": unknown reason");

    // Create resource object
    std::vector<GLfloat> positions;
    std::vector<GLfloat> normals;
    std::vector<GLfloat> uvs;

    GLuint lastIndex = 0;
    std::vector<GLuint> indices;
    std::unordered_map<std::string, GLuint> visitedVertices;

    // Engine policy: OBJ name must match provided name
    bool shapeFound = false;

    for(const tinyobj::shape_t& shape : vShapes)
    {
        if(shape.name == name)
            shapeFound = true;
        else
            continue;

        for(tinyobj::index_t index : shape.mesh.indices)
        {
            if(index.vertex_index < 0)
                THROW_EXCEPTION("Missing position index in vertex");
            if(index.normal_index < 0)
                THROW_EXCEPTION("Missing normal index in vertex");
            if(index.texcoord_index < 0)
                THROW_EXCEPTION("Missing UV index in vertex");

            // For each vertex, compute a unique id based on its multiple attribute indices
            // TODO improve performance of UIDs
            std::stringstream ss;
            ss << index.vertex_index << "-" << index.normal_index << "-" << index.texcoord_index;
            std::string vertexUID = ss.str();

            if(visitedVertices.count(vertexUID))
            {
                // Use existing vertex
                GLuint prevIndex = visitedVertices.at(vertexUID);
                indices.push_back(prevIndex);
            }
            else
            {
                // Create new vertex
                indices.push_back(lastIndex);
                visitedVertices[vertexUID] = lastIndex;
                ++lastIndex;

                // Add geometry data for the vertex
                for(int i = 0; i < 3; ++i)
                    positions.push_back(attributes.vertices[3 * index.vertex_index + i]);
                for(int i = 0; i < 3; ++i)
                    normals.push_back(attributes.normals[3 * index.normal_index + i]);
                for(int i = 0; i < 2; ++i)
                    uvs.push_back(attributes.texcoords[2 * index.texcoord_index + i]);
            }
        }

        break;
    }

    if(!shapeFound)
        THROW_EXCEPTION("Could not find object named '" << name << "' in .obj file " << fileName);

    // Add mesh geometry to VBO
    RenderManager& renderMgr = Engine::getInstance().getRenderManager();
    GLuint offsetInBuffer = renderMgr.appendMesh(positions, normals, uvs, indices, lastIndex);

    // Create new mesh resource
    MeshResource* newMesh = new MeshResource(
        id,
        name,
        positions,
        normals,
        uvs,
        indices,
        offsetInBuffer
    );

    // Return new resource
    return Resource_sptr(newMesh);
}

}
