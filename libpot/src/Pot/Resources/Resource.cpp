#include <Pot/Resources/Resource.h>

namespace Pot
{

Resource::Resource(uint32_t id, Type type, const std::string& name)
    : id(id),
      type(type),
      name(name)
{
    if(name.empty())
        THROW_EXCEPTION("Resource cannot have an empty name");
}

Resource::~Resource()
{
}

}
