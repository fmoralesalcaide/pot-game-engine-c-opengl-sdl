#include <Pot/Managers/CollisionManager/CollisionResult.h>

namespace Pot
{

CollisionResult::CollisionResult()
    : colliding(false)
{
}

CollisionResult::~CollisionResult()
{
}

bool CollisionResult::isColliding() const
{
    return colliding;
}

void CollisionResult::setColliding(bool collide)
{
    this->colliding = collide;
}

}

