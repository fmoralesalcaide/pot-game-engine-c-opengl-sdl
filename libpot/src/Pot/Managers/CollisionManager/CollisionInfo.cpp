#include <Pot/Managers/CollisionManager/CollisionInfo.h>

namespace Pot
{

CollisionInfo::CollisionInfo(Components::Collider* collidingCollider)
    : collidingCollider(collidingCollider)
{
}

CollisionInfo::~CollisionInfo()
{
}

Components::Collider* CollisionInfo::getCollidingCollider() const
{
    return collidingCollider;
}

}
