#include <Pot/Managers/ResourceManager.h>

namespace Pot
{

ResourceManager::ResourceManager()
    : Manager(Type::RESOURCE),
      nextResourceId(0)
{
}

ResourceManager::~ResourceManager()
{
}

void ResourceManager::initialize()
{

}

void ResourceManager::finish()
{
    // Remove all types of resources
    removeResources(Resource::Type::MESH);
}

bool ResourceManager::hasResource(Resource::Type type, const std::string& name)
{
    auto& resourceMap = getResourceMap(type);
    return resourceMap.count(name);
}

Resource_sptr ResourceManager::loadResource(Resource::Type type, const std::string& name, const std::string& fileName)
{
    auto& resourceMap = getResourceMap(type);
    ResourceFactory* resourceTypeFactory = getResourceFactory(type);

    if(resourceMap.count(name))
        THROW_EXCEPTION("Resource with name " << name << " already loaded");

    uint32_t resourceId = nextResourceId++;
    resourceMap[name] = resourceTypeFactory->loadResource(resourceId, name, fileName);
    return resourceMap.at(name);
}

Resource_sptr ResourceManager::getResource(Resource::Type type, const std::string& name)
{
    if(!hasResource(type, name))
        THROW_EXCEPTION("Resource " << name << " is not loaded");

    auto& resourceMap = getResourceMap(type);
    return resourceMap.at(name);
}

void ResourceManager::removeResource(Resource::Type type, const std::string& name)
{
    if(!hasResource(type, name))
        THROW_EXCEPTION("Resource " << name << " is not loaded");

    auto& resourceMap = getResourceMap(type);
    unsigned int resourceUseCount = resourceMap.at(name).use_count() - 1;

    if(resourceUseCount)
        THROW_EXCEPTION("Resource " << name << " is being used by " << resourceUseCount << " element(s)");

    resourceMap.erase(name);
}

void ResourceManager::removeResources(Resource::Type type)
{
    auto& resourceMap = getResourceMap(type);
    while(!resourceMap.empty())
        removeResource(type, resourceMap.begin()->first);
}

ResourceFactory* ResourceManager::getResourceFactory(Resource::Type type)
{
    switch (type)
    {
    case Resource::Type::MESH:
        return &meshResourceFactory;
    default:
        THROW_EXCEPTION("Resource of type" << short(type) << " not implemented");
        break;
    }
}

std::unordered_map<std::string, Resource_sptr>& ResourceManager::getResourceMap(Resource::Type type)
{
    switch (type)
    {
    case Resource::Type::MESH:
        return meshResources;
    default:
        THROW_EXCEPTION("Resource of type" << short(type) << " not implemented");
        break;
    }
}

}
