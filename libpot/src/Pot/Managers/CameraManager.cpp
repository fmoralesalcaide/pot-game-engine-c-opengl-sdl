#include <Pot/Managers/CameraManager.h>

namespace Pot
{

CameraManager::CameraManager()
    : Manager(Manager::Type::CAMERA),
      activeCamera(nullptr)
{
}

CameraManager::~CameraManager()
{
}

void CameraManager::finish()
{
    if(!cameras.empty())
        THROW_EXCEPTION("Found existing cameras while destroying CameraManager");
}

bool CameraManager::hasActiveCamera() const
{
    return activeCamera;
}

Components::Camera* CameraManager::getActiveCamera()
{
    if(!activeCamera)
        THROW_EXCEPTION("Cannot return null active camera");
    return activeCamera;
}

void CameraManager::setActiveCamera(uint32_t componentId)
{
    if(!cameras.count(componentId))
        THROW_EXCEPTION("Camera with component id " << componentId << "not in CameraManager");
    activeCamera = cameras.at(componentId);
}

void CameraManager::unsetActiveCamera()
{
    activeCamera = nullptr;
}

void CameraManager::process()
{
    if(activeCamera)
        activeCamera->updateViewProjMatrix();
}

void CameraManager::registerCamera(uint32_t id, Components::Camera* camera)
{
    if(!camera)
        THROW_EXCEPTION("Cannot register null camera to CameraManager");
    if(cameras.count(id))
        THROW_EXCEPTION("Cannot register twice the same camera " << id << " to CameraManager");
    cameras[id] = camera;

    // By default, set as active the first camera
    if(cameras.size() == 1)
        setActiveCamera(id);
}

void CameraManager::unregisterCamera(uint32_t id)
{
    if(!cameras.count(id))
        THROW_EXCEPTION("Cannot unregister unregistered camera " << id << " from CameraManager");
    cameras.erase(id);
}

}
