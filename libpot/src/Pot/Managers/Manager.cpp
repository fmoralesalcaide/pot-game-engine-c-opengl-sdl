#include <Pot/Managers/Manager.h>

namespace Pot
{
Manager::Manager(Manager::Type type)
    : type(type)
{
}

Manager::~Manager()
{
}

void Manager::initialize()
{
    // Nothing to do by default
}

void Manager::finish()
{
    // Nothin to do by default
}
}
