#include <Pot/Managers/ComponentManager.h>

namespace Pot
{

ComponentManager::ComponentManager()
    : Manager(Manager::Type::COMPONENT),
      nextComponentId(0)
{
}

ComponentManager::~ComponentManager()
{
}

void ComponentManager::initialize()
{
}

void ComponentManager::finish()
{
}

void ComponentManager::removeAllComponents(GameObject* parentGameObject)
{
    if(!parentGameObject)
        THROW_EXCEPTION("Cannot add Component to a null GameObject");

    std::vector<Component*> gameObjectComponents = parentGameObject->getAllComponents();

    for(Component* component : gameObjectComponents)
    {
        uint32_t id = component->getId();
        delete components.at(id);
        components.erase(id);
    }

    parentGameObject->notifyAllComponentsRemoved();
}

Component* ComponentManager::getComponent(uint32_t id)
{
    if(!components.count(id))
        THROW_EXCEPTION("Component with id " << id << " not existing in ComponentManager");
    return components.at(id);
}

}
