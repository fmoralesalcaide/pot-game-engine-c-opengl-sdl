#include <Pot/Engine.h>
#include <Pot/Managers/RenderManager/ShaderParser.h>
#include <Pot/Managers/RenderManager.h>
#include <Pot/Managers/ResourceManager.h>
#include <Pot/Resources/MeshResource.h>

namespace Pot
{

RenderManager::RenderManager()
    : Manager(Manager::Type::RENDER),
      sdlWindow(nullptr),
      glContext(nullptr),
      windowWidth(800),
      windowHeight(600),
      positionBuffer(0),
      normalBuffer(0),
      uvBuffer(0),
      elementBuffer(0),
      currIndices(0),
      currPositionBufferOffset(0),
      currNormalBufferOffset(0),
      currUVBufferOffset(0),
      currElementBufferOffset(0)
{
}

RenderManager::~RenderManager()
{
}

void RenderManager::initialize()
{
    // Initialize SDL video subsystem
    if(SDL_Init( SDL_INIT_VIDEO ) < 0)
        THROW_EXCEPTION("Video initialization failed: " << SDL_GetError());

    // Create OpenGL window
    sdlWindow = SDL_CreateWindow(
                    "PotEngine test window",
                    SDL_WINDOWPOS_CENTERED,
                    SDL_WINDOWPOS_CENTERED,
                    windowWidth,
                    windowHeight,
                    SDL_WINDOW_OPENGL
                );

    SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 16);
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

    initializeOpenGL();
}

SDL_Window* RenderManager::getSDLWindowHandle() const
{
    return sdlWindow;
}

void RenderManager::finish()
{
    clearBuffers();
    finishOpengl();

    SDL_GL_DeleteContext(glContext);
    SDL_DestroyWindow(sdlWindow);
    SDL_QuitSubSystem(SDL_INIT_VIDEO);
}

std::tuple<int,int> RenderManager::getWindowSize() const
{
    return std::make_tuple(windowWidth, windowHeight);
}

void RenderManager::render()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    if(Engine::getInstance().getCameraManager().hasActiveCamera())
    {
        for(auto it : receivers)
            it.second->onRender();
    }

    // Render gizmos
    // TODO fix this with a specific gizmo shader
    glm::mat4 identity(1);
    const glm::mat4& viewProjMatrix = Engine::getInstance().getCameraManager().getActiveCamera()->getViewProjMatrix();
    glUniformMatrix4fv(0, 1, GL_FALSE, &identity[0][0]);
    glUniformMatrix4fv(1, 1, GL_FALSE, &viewProjMatrix[0][0]);

    Engine::getInstance().getCollisionManager().renderOctree();

    SDL_GL_SwapWindow(sdlWindow);
}

void RenderManager::registerReceiver(uint32_t id, IRenderReceiver* receiver)
{
    if(!receiver)
        THROW_EXCEPTION("Cannot register null receiver to VideoManager");
    if(receivers.count(id))
        THROW_EXCEPTION("Cannot register receiver " << id << " to VideoManager twice");
    receivers[id] = receiver;
}

void RenderManager::unregisterReceiver(uint32_t id)
{
    if(!receivers.count(id))
        THROW_EXCEPTION("Cannot unregister unregistered receiver " << id << " from VideoManager");
    receivers.erase(id);
}


GLuint RenderManager::getShaderDescriptor(const std::string& name) const
{
    if(!shaderPrograms.count(name))
        THROW_EXCEPTION("Shader program with name " << name << " not loaded in RenderManager");

    return shaderPrograms.at(name);
}

void RenderManager::loadShaders(const std::string& shadersFile)
{
    Pot::XMLTools::XMLParser xmlParser;
    xmlParser.parseData<std::unordered_map<std::string, GLuint>, ShaderParser>(&shaderPrograms, shadersFile);
}

GLuint RenderManager::appendMesh(const std::vector<GLfloat>& positions, const std::vector<GLfloat>& normals, const std::vector<GLfloat>& uvs, const std::vector<GLuint>& indices, uint32_t numIndices)
{
    // Append data
    uint32_t sizeBytes = positions.size() * sizeof(GLfloat);
    glBindBuffer(GL_ARRAY_BUFFER, positionBuffer);
    glBufferSubData(GL_ARRAY_BUFFER, currPositionBufferOffset, sizeBytes, positions.data());
    currPositionBufferOffset += sizeBytes;

    sizeBytes = normals.size() * sizeof(GLfloat);
    glBindBuffer(GL_ARRAY_BUFFER, normalBuffer);
    glBufferSubData(GL_ARRAY_BUFFER, currNormalBufferOffset, sizeBytes, normals.data());
    currNormalBufferOffset += sizeBytes;

    sizeBytes = uvs.size() * sizeof(GLfloat);
    glBindBuffer(GL_ARRAY_BUFFER, uvBuffer);
    glBufferSubData(GL_ARRAY_BUFFER, currUVBufferOffset, sizeBytes, uvs.data());
    currUVBufferOffset += sizeBytes;

    // Shift indices
    std::vector<GLuint> shiftedIndices = indices;
    for(GLuint& shiftedIndex : shiftedIndices)
        shiftedIndex += currIndices;
    currIndices += numIndices;

    // Append indices to element buffer
    GLuint offsetInBuffer = currElementBufferOffset;

    sizeBytes = shiftedIndices.size() * sizeof(GLuint);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementBuffer);
    glBufferSubData(GL_ELEMENT_ARRAY_BUFFER, currElementBufferOffset, sizeBytes, shiftedIndices.data());
    currElementBufferOffset += sizeBytes;

    // Store mesh and return identifier
    return offsetInBuffer;
}

void RenderManager::renderMesh(Resource_sptr mesh)
{
    MeshResource* m = dynamic_cast<MeshResource*>(mesh.get());
    if(!m)
        THROW_EXCEPTION("Could not convert Resource_sptr to MeshResource* before rendering");

    GLuint elements = m->getIndices().size();
    GLuint offset = m->getOffsetInBuffer();

    glDrawElements(GL_TRIANGLES, elements, GL_UNSIGNED_INT, BUFFER_OFFSET(offset));
}

void RenderManager::clearBuffers()
{
    // Clear all mesh resources, or ensure that are deleted before this call
    resetGeometryBuffers();
}

void RenderManager::initializeOpenGL()
{
    // Create OpenGL context
    glContext = SDL_GL_CreateContext(sdlWindow);

    // Initialize GLEW
    glewInit();

    // Set background color
    glClearColor(0.9, 0.9, 0.9, 1);
    // Enable depth test
    glEnable(GL_DEPTH_TEST);
    // Accept fragment if it closer to the camera than the former one
    glDepthFunc(GL_LESS);

    // Cull triangles which normal is not towards the camera
    glEnable(GL_CULL_FACE);

    // Initialize buffers
    glGenBuffers(1, &positionBuffer);
    glGenBuffers(1, &normalBuffer);
    glGenBuffers(1, &uvBuffer);
    glGenBuffers(1, &elementBuffer);

    // Set buffer size to specified video memory
    resetGeometryBuffers();
}

void RenderManager::finishOpengl()
{
    glDeleteBuffers(1, &positionBuffer);
    glDeleteBuffers(1, &normalBuffer);
    glDeleteBuffers(1, &uvBuffer);
    glDeleteBuffers(1, &elementBuffer);
}

void RenderManager::resetGeometryBuffers()
{
    // TODO read this value from custom parameter
    uint32_t bufferSizeBytes = 1e6 * 8; // 8 MB

    glBindBuffer(GL_ARRAY_BUFFER, positionBuffer);
    glBufferData(GL_ARRAY_BUFFER, bufferSizeBytes, nullptr, GL_STATIC_DRAW);

    glBindBuffer(GL_ARRAY_BUFFER, normalBuffer);
    glBufferData(GL_ARRAY_BUFFER, bufferSizeBytes, nullptr, GL_STATIC_DRAW);

    glBindBuffer(GL_ARRAY_BUFFER, uvBuffer);
    glBufferData(GL_ARRAY_BUFFER, bufferSizeBytes, nullptr, GL_STATIC_DRAW);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementBuffer);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, bufferSizeBytes, nullptr , GL_STATIC_DRAW);

    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

}
