#include <Pot/Managers/UpdateManager.h>

namespace Pot
{
UpdateManager::UpdateManager()
    : Manager(Manager::Type::UPDATE)
{
}

UpdateManager::~UpdateManager()
{
}

void UpdateManager::initialize()
{
}

void UpdateManager::finish()
{
    if(!receivers.empty())
        THROW_EXCEPTION("Found unregistered receivers while destroying UpdateManager");
}

void UpdateManager::update(double deltaTime)
{
    for(auto it : receivers)
        it.second->onUpdate(deltaTime);
}

void UpdateManager::registerReceiver(uint32_t id, IUpdateReceiver* receiver)
{
    if(!receiver)
        THROW_EXCEPTION("Cannot register null receiver to UpdateManager");
    if(receivers.count(id))
        THROW_EXCEPTION("Cannot register receiver " << id << " to UpdateManager twice");
    receivers[id] = receiver;
}

void UpdateManager::unregisterReceiver(uint32_t id)
{
    if(!receivers.count(id))
        THROW_EXCEPTION("Cannot unregister unregistered receiver " << id << " from UpdateManager");
    receivers.erase(id);
}

}

