#include <Pot/Managers/RenderManager/ShaderParser.h>

namespace Pot
{

ShaderParser::ShaderParser(std::unordered_map<std::string, GLuint>* shaderPrograms)
    : currProgram(-1),
      currVertexShader(-1),
      currFragmentShader(-1),
      programs(shaderPrograms),
      fsm(STATE_COUNT, EVENT_COUNT, STATE_NOTHING_FOUND, STATE_PARSING_ERROR, 0),
      fsmInstance(&fsm)
{
    fsm.addTransition(STATE_NOTHING_FOUND, EVENT_START_CONFIG, STATE_INSIDE_CONFIG);
    fsm.addTransition(STATE_INSIDE_CONFIG, EVENT_START_PROGRAM, STATE_INSIDE_PROGRAM);
    fsm.addTransition(STATE_INSIDE_PROGRAM, EVENT_START_SHADER, STATE_INSIDE_SHADER);
    fsm.addTransition(STATE_INSIDE_SHADER, EVENT_END_SHADER, STATE_INSIDE_PROGRAM);
    fsm.addTransition(STATE_INSIDE_PROGRAM, EVENT_END_PROGRAM, STATE_INSIDE_CONFIG);
    fsm.addTransition(STATE_INSIDE_CONFIG, EVENT_END_CONFIG, STATE_PARSING_COMPLETED);
    fsmInstance.reset();
}

ShaderParser::~ShaderParser()
{
}

void ShaderParser::startElement(const XMLCh* const uri, const XMLCh* const localname, const XMLCh* const qname, const xercesc::Attributes& attrs)
{
    // Unused parameters
    (void) uri;
    (void) localname;
    strNodeContent = "";
    std::string strQName(Pot::XMLTools::XMLTranscoders::transcodeXMLChars2String(qname));
    std::size_t state = fsmInstance.getCurrentState();

    switch(state)
    {
    case STATE_PARSING_ERROR:
    {
        THROW_EXCEPTION("Found element in the wrong place '" << strQName << "'");
        break;
    }

    default:
        break;
    }

    if(!strQName.compare("ShaderConfiguration"))
        fsmInstance.signalEvent(EVENT_START_CONFIG);
    else if(!strQName.compare("Program"))
        fsmInstance.signalEvent(EVENT_START_PROGRAM);
    else if(!strQName.compare("Shader"))
        fsmInstance.signalEvent(EVENT_START_SHADER);
    else
        THROW_EXCEPTION("Found unknown element '" << strQName << "'");

    state = fsmInstance.getCurrentState();

    switch(state)
    {
    case STATE_INSIDE_CONFIG:
        break;

    case STATE_INSIDE_PROGRAM:
    {
        std::string name = Pot::XMLTools::XMLAttributesReader::getAttributeValue<std::string>(attrs, "name");

        if(programs->count(name))
            THROW_EXCEPTION("Shader program named " << name << "already registered");

        currProgram = glCreateProgram();
        (*programs)[name] = currProgram;

        currVertexShader = -1;
        currFragmentShader = -1;

        break;
    }

    case STATE_INSIDE_SHADER:
    {
        // 1. Parse shader type
        std::string type = Pot::XMLTools::XMLAttributesReader::getAttributeValue<std::string>(attrs, "type");
        GLuint currShader;

        if(type == "vertex")
        {
            currVertexShader = glCreateShader(GL_VERTEX_SHADER);
            currShader = currVertexShader;
        }
        else if(type == "fragment")
        {
            currFragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
            currShader = currFragmentShader;
        }
        else
            THROW_EXCEPTION("Fragment type " << type << " not implemented");

        // 2. Parse shader source code
        std::string sourceCodeFile = Pot::XMLTools::XMLAttributesReader::getAttributeValue<std::string>(attrs, "file");
        std::ifstream ifs(sourceCodeFile, std::ios::in);

        if(!ifs.is_open())
            THROW_EXCEPTION("Could not open shader source code file " << sourceCodeFile << " for parsing");

        std::string sourceCodeStr = std::string();
        std::string line = "";

        while(getline(ifs, line))
            sourceCodeStr += '\n' + line;

        ifs.close();

        // 3. Compile source code
        const GLchar* shaderSourceStrPtr = sourceCodeStr.c_str();
        glShaderSource(currShader, 1, &shaderSourceStrPtr , nullptr);
        glCompileShader(currShader);

        // 4. Check result
        int msgLen = 0;
        GLint result = GL_FALSE;

        glGetShaderiv(currShader, GL_COMPILE_STATUS, &result);
        glGetShaderiv(currShader, GL_INFO_LOG_LENGTH, &msgLen);

        if(!result)
        {
            std::string message("", msgLen);
            glGetShaderInfoLog(currShader, msgLen, nullptr, &message[0]);
            THROW_EXCEPTION("Error while compiling shader in " << sourceCodeFile << ":\n" << message);
        }

        break;
    }

    case STATE_PARSING_ERROR:
        THROW_EXCEPTION("Found element in the wrong place '" << strQName << "'");
        break;

    default:
        break;
    }
}

void ShaderParser::characters(const XMLCh* const chars, const XMLSize_t length)
{
    if(length > 0)
        strNodeContent += Pot::XMLTools::XMLTranscoders::transcodeXMLChars2String(chars, length);
}

void ShaderParser::endElement(const XMLCh* const uri, const XMLCh* const localname, const XMLCh* const qname)
{
    // Unused parameters
    (void) uri;
    (void) localname;
    std::string strQName(Pot::XMLTools::XMLTranscoders::transcodeXMLChars2String(qname));
    size_t state = fsmInstance.getCurrentState();

    switch(state)
    {
    case STATE_PARSING_ERROR:
        THROW_EXCEPTION("Found element in the wrong place '" << strQName << "'");
        break;

    case STATE_INSIDE_CONFIG:
        break;

    case STATE_INSIDE_PROGRAM:
    {
        if(currVertexShader < 0)
            THROW_EXCEPTION("Cannot compile shader program " << currProgram << ", missing vertex shader");
        if(currFragmentShader < 0)
            THROW_EXCEPTION("Cannot compile shader program " << currProgram << ", missing fragment shader");

        glAttachShader(currProgram, currVertexShader);
        glAttachShader(currProgram, currFragmentShader);
        glLinkProgram(currProgram);

        // Check the program
        int msgLen = 0;
        GLint result = GL_FALSE;

        glGetProgramiv(currProgram, GL_LINK_STATUS, &result);
        glGetProgramiv(currProgram, GL_INFO_LOG_LENGTH, &msgLen);

        if(!result)
        {
            std::string message("", msgLen);
            glGetProgramInfoLog(currProgram, msgLen, nullptr, &message[0]);
            THROW_EXCEPTION("Error while linking shaders to program " << currProgram << ":\n" << message);
        }

        glDetachShader(currProgram, currVertexShader);
        glDetachShader(currProgram, currFragmentShader);

        glDeleteShader(currVertexShader);
        glDeleteShader(currFragmentShader);

        break;
    }

    case STATE_INSIDE_SHADER:
        break;

    default:
        break;
    }

    if(!strQName.compare("ShaderConfiguration"))
        fsmInstance.signalEvent(EVENT_END_CONFIG);
    else if(!strQName.compare("Program"))
        fsmInstance.signalEvent(EVENT_END_PROGRAM);
    else if(!strQName.compare("Shader"))
        fsmInstance.signalEvent(EVENT_END_SHADER);
    else
        THROW_EXCEPTION("Found unknown element '" << strQName << "'");

    state = fsmInstance.getCurrentState();

    switch(state)
    {
    case STATE_PARSING_ERROR:
        THROW_EXCEPTION("Found element in the wrong place '" << strQName << "'");
        break;

    default:
        break;
    }
}

}
