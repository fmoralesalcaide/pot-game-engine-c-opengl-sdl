#include <Pot/Managers/CollisionManager.h>
#include <Pot/Engine.h>
#include <Pot/Managers/ComponentManager.h>

#include <Pot/Components/Collider.h>
#include <Pot/Managers/CollisionManager/CollisionResult.h>
#include <Pot/Managers/CollisionManager/CollisionInfo.h>

#include <Pot/Components/Colliders/SphereCollider.h>
#include <Pot/Components/Colliders/PlaneCollider.h>

namespace Pot
{

CollisionManager::CollisionManager()
    : Manager(Manager::Type::COLLISION)
{
}

CollisionManager::~CollisionManager()
{
}

void CollisionManager::initialize()
{
}

void CollisionManager::finish()
{
    if(!collisionReceivers.empty())
        THROW_EXCEPTION("Found unregistered receivers while destroying CollisionManager");
}

void CollisionManager::processCollisions()
{
    for(auto keyValA : colliders)
        octree.update(keyValA.second);
    octree.postUpdates();

    // TODO check collisions using octree
}

void CollisionManager::registerCollider(uint32_t id, Components::Collider* collider)
{
    if(!collider)
        THROW_EXCEPTION("Cannot register null collider to CollisionManager");
    if(colliders.count(id))
        THROW_EXCEPTION("Cannot register twice the same collider " << id << " to CollisionManager");

    colliders[id] = collider;
    octree.insert(collider);
}

void CollisionManager::unregisterCollider(uint32_t id)
{
    if(!colliders.count(id))
        THROW_EXCEPTION("Cannot unregister unregistered collider " << id << " from CollisionManager");

    octree.remove(colliders.at(id));
    colliders.erase(id);
}

void CollisionManager::registerCollisionReceiver(uint32_t id, uint32_t gameObjectId, ICollisionReceiver* receiver)
{
    if(!receiver)
        THROW_EXCEPTION("Cannot register null collision receiver to CollisionManager");
    if(collisionReceivers.count(id))
        THROW_EXCEPTION("Cannot register same collision receiver " << id << " to CollisionManager");

    collisionReceivers[id] = receiver;
    collisionReceiversByGameObject[gameObjectId].insert(id);
}

void CollisionManager::unregisterCollisionReceiver(uint32_t id, uint32_t gameObjectId)
{
    if(!collisionReceivers.count(id))
        THROW_EXCEPTION("Cannot unregister unregistered collision receiver " << id << " from CollisionManager");

    collisionReceiversByGameObject.at(gameObjectId).erase(id);
    collisionReceivers.erase(id);
}

void CollisionManager::renderOctree()
{
    octree.render();
}

uint32_t CollisionManager::getGameObjectId(uint32_t componentId)
{
    ComponentManager& componentMgr = Engine::getInstance().getComponentManager();
    return componentMgr.getComponent(componentId)->getGameObject()->getId();
}

void CollisionManager::checkColliders(Components::Collider* colA, Components::Collider* colB)
{
    // Ignore collision if the colliders are the same or belong to the same game object
    // Note: colliders sharing a common ancestor GameObject will collide.

    bool skip = false;

    uint32_t colAId = colA->getId();
    uint32_t colBId = colB->getId();

    uint32_t gameObjectAId = colA->getGameObject()->getId();
    uint32_t gameObjectBId = colB->getGameObject()->getId();

    skip = skip || colAId >= colBId; // ">" is to avoid duplicate comparisons
    skip = skip || gameObjectAId == gameObjectBId;

    if(skip)
        return;

    CollisionResult result = computeCollision(colA, colB);

    uint64_t key = uint64_t(colAId) << 32 || uint64_t(colBId);
    bool wasColliding = currentCollisions.count(key);
    bool isColliding = result.isColliding();

    if(!wasColliding && !isColliding)
        return;

    const std::set<uint32_t>& colReceiversA = collisionReceiversByGameObject.at(gameObjectAId);
    const std::set<uint32_t>& colReceiversB = collisionReceiversByGameObject.at(gameObjectBId);

    CollisionInfo infoForA(colB);
    CollisionInfo infoForB(colA);

    // Collision enter
    if(!wasColliding && isColliding)
    {
        for(uint32_t colReceiverA : colReceiversA)
            collisionReceivers.at(colReceiverA)->onCollisionEnter(infoForA);
        for(uint32_t colReceiverB : colReceiversB)
            collisionReceivers.at(colReceiverB)->onCollisionEnter(infoForB);

        currentCollisions.insert(key);
    }
    // Collision stay
    else if(wasColliding && isColliding)
    {
        for(uint32_t colReceiverA : colReceiversA)
            collisionReceivers.at(colReceiverA)->onCollisionStay(infoForA);
        for(uint32_t colReceiverB : colReceiversB)
            collisionReceivers.at(colReceiverB)->onCollisionStay(infoForB);
    }
    // Collision leave
    else if(wasColliding && !isColliding)
    {
        for(uint32_t colReceiverA : colReceiversA)
            collisionReceivers.at(colReceiverA)->onCollisionLeave();
        for(uint32_t colReceiverB : colReceiversB)
            collisionReceivers.at(colReceiverB)->onCollisionLeave();

        currentCollisions.erase(key);
    }
}

CollisionResult CollisionManager::computeCollision(Components::Collider* colA, Components::Collider* colB)
{
    CollisionResult result;

    Components::Collider::Type typeA = colA->getType();
    Components::Collider::Type typeB = colB->getType();

    if(typeA == Components::Collider::Type::SPHERE && typeB == Components::Collider::Type::SPHERE)
    {
        Components::SphereCollider* sphereA = (Components::SphereCollider*) colA;
        Components::SphereCollider* sphereB = (Components::SphereCollider*) colB;

        double distance = glm::length(sphereA->getWorldPosition() - sphereB->getWorldPosition());
        result.setColliding(distance <= sphereA->getRadius() + sphereB->getRadius());
    }
    else
        THROW_EXCEPTION("Collision computation not implemented");

    return result;
}

}
