#include <Pot/Engine.h>
#include <Pot/Managers/ComponentManager.h>
#include <Pot/Managers/GameObjectManager.h>

namespace Pot
{

GameObjectManager::GameObjectManager()
    : Manager(Manager::Type::GAMEOBJECT),
      nextGameObjectId(0)
{
}

GameObjectManager::~GameObjectManager()
{
}

void GameObjectManager::initialize()
{
}

void GameObjectManager::finish()
{
    // Destroy all remaining game objects
    for(auto keyVal : gameObjects)
        addToDeletionList(keyVal.second);

    processDeletionList();
}

GameObject* GameObjectManager::instantiateGameObject()
{
    GameObject* gameObject = new GameObject(nextGameObjectId);
    gameObjects[nextGameObjectId] = gameObject;
    ++nextGameObjectId;
    return gameObject;
}

void GameObjectManager::addToDeletionList(GameObject* gameObject)
{
    if(!gameObject)
        THROW_EXCEPTION("Cannot destroy a null GameObject");

    gameObjectsToRemove.insert(gameObject->getId());
}

void GameObjectManager::processDeletionList()
{
    for(uint32_t gameObjectId : gameObjectsToRemove)
    {
        GameObject * gameObject = gameObjects.at(gameObjectId);

        if(gameObject->hasParent())
            gameObject->detachFromParent();

        if(gameObject->hasChildren())
            gameObject->detachChildren();

        ComponentManager& componentMgr = Engine::getInstance().getComponentManager();
        componentMgr.removeAllComponents(gameObject);

        gameObjects.erase(gameObjectId);
        delete gameObject;
    }

    gameObjectsToRemove.clear();
}

}
