#include <Pot/IComponentReceivers/IMouseReceiver.h>
#include <Pot/IComponentReceivers/IKeyboardReceiver.h>
#include <Pot/Engine.h>
#include <Pot/Managers/InputManager.h>

namespace Pot
{

InputManager::InputManager()
    : Manager(Manager::Type::INPUT),
      pressedKeyMap(nullptr)
{
}

InputManager::~InputManager()
{
}

void InputManager::initialize()
{
    // Initialize SDL video subsystem
    if(SDL_Init(SDL_INIT_EVENTS ) < 0)
        THROW_EXCEPTION("Events initialization failed: " << SDL_GetError());
}

void InputManager::finish()
{
    if(!mouseReceivers.empty())
        THROW_EXCEPTION("Found unregistered receivers while destroying InputManager");

    SDL_QuitSubSystem(SDL_INIT_EVENTS);
}

void InputManager::processInput()
{
    SDL_Event sdlEvent;
    bool sdlQuitEventReceived = false;

    pressedKeyMap = SDL_GetKeyboardState(nullptr);
    while(!sdlQuitEventReceived && SDL_PollEvent(&sdlEvent))
    {
        void (IMouseReceiver::* mouseCbk)(void) = nullptr;
        void (IKeyboardReceiver::* keyboardCbk)(void) = nullptr;

        switch (sdlEvent.type)
        {
        // Mouse input events
        case SDL_MOUSEBUTTONUP:
            mouseCbk = &IMouseReceiver::onMouseUp;
            break;
        case SDL_MOUSEBUTTONDOWN:
            mouseCbk = &IMouseReceiver::onMouseDown;
            break;
        case SDL_MOUSEMOTION:
            mouseCbk = &IMouseReceiver::onMouseMove;
            break;
        case SDL_MOUSEWHEEL:
            mouseCbk = &IMouseReceiver::onMouseScroll;
            break;

        // Keyboard input events
        case SDL_KEYUP:
            keyboardCbk = &IKeyboardReceiver::onKeyUp;
            break;
        case SDL_KEYDOWN:
            keyboardCbk = &IKeyboardReceiver::onKeyDown;
            break;

        // SDL window closed
        case SDL_QUIT:
            sdlQuitEventReceived = true;
            break;
        default:
            break;
        }

        if(mouseCbk)
        {
            for(auto it : mouseReceivers)
                (it.second->*mouseCbk)();
        }
        if(keyboardCbk)
        {
            for(auto it : keyboardReceivers)
                (it.second->*keyboardCbk)();
        }
    }

    if(sdlQuitEventReceived)
        Engine::getInstance().stop();
}

void InputManager::registerMouseReceiver(uint32_t id, IMouseReceiver* receiver)
{
    if(!receiver)
        THROW_EXCEPTION("Cannot register null mouse receiver to InputManager");
    if(mouseReceivers.count(id))
        THROW_EXCEPTION("Cannot register same mouse receiver " << id << " to InputManager");
    mouseReceivers[id] = receiver;
}

void InputManager::unregisterMouseReceiver(uint32_t id)
{
    if(!mouseReceivers.count(id))
        THROW_EXCEPTION("Cannot unregister unregistered mouse receiver " << id << " from InputManager");
    mouseReceivers.erase(id);
}

void InputManager::registerKeyboardReceiver(uint32_t id, IKeyboardReceiver* receiver)
{
    if(!receiver)
        THROW_EXCEPTION("Cannot register null keyboard receiver to InputManager");
    if(keyboardReceivers.count(id))
        THROW_EXCEPTION("Cannot register twice keyboard receiver " << id << " to InputManager");
    keyboardReceivers[id] = receiver;
}

void InputManager::unregisterKeyboardReceiver(uint32_t id)
{
    if(!keyboardReceivers.count(id))
        THROW_EXCEPTION("Cannot unregister unregistered keyboard receiver " << id << " from InputManager");
    keyboardReceivers.erase(id);
}

bool InputManager::isKeyPressed(const SDL_Scancode& key) const
{
    return pressedKeyMap[key];
}

std::tuple<int,int> InputManager::getMousePos() const
{
    int x, y;
    SDL_GetMouseState(&x, &y);
    return std::make_tuple(x, y);
}

std::tuple<int,int> InputManager::getMouseDelta() const
{
    int dx, dy;
    SDL_GetRelativeMouseState(&dx, &dy);
    return std::make_tuple(dx, dy);
}

void InputManager::lockMouse()
{
    SDL_SetRelativeMouseMode(SDL_TRUE);
}

void InputManager::unlockMouse()
{
    SDL_SetRelativeMouseMode(SDL_FALSE);
}

}
