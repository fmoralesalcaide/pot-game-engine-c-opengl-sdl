#include <Pot/Components/Component.h>
#include <Pot/GameObject.h>

namespace Pot
{

Component::Component(GameObject* gameObject, uint32_t id, const std::string& name)
    : id(id),
      name(name),
      gameObject(gameObject)
{
    if(name.empty())
        THROW_EXCEPTION("Component name cannot be empty");
}

Component::~Component()
{
}

bool Component::isUniqueType()
{
    // Unique by default
    return true;
}

void Component::initialize()
{
    // Nothing by default
}

void Component::finish()
{
    // Nothing by default
}

}
