#include <Pot/Components/Colliders/SphereCollider.h>

namespace Pot
{

namespace Components
{

SphereCollider::SphereCollider(GameObject* gameObject, uint32_t id, const std::string& name)
    : Collider(Collider::Type::SPHERE, gameObject, id, name),
      radius(0)
{
}

SphereCollider::~SphereCollider()
{
}

void SphereCollider::setRadius(double radius)
{
    if(radius <= 0)
        THROW_EXCEPTION("SphereCollider radius must be positive, " << radius << " provided");
    this->radius = radius;
}

}

}
