#include <Pot/Components/Colliders/PlaneCollider.h>

namespace Pot
{

namespace Components
{

PlaneCollider::PlaneCollider(GameObject* gameObject, uint32_t id, const std::string& name)
    : Collider(Collider::Type::PLANE, gameObject, id, name),
      normal(0,1,0)
{
}

PlaneCollider::~PlaneCollider()
{
}

}

}
