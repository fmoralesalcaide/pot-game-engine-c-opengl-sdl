#include <Pot/Components/CameraController.h>
#include <Pot/Engine.h>
#include <Pot/GameObject.h>
#include <Pot/Components/Transform.h>

namespace Pot
{

namespace Components
{

CameraController::CameraController(Pot::GameObject* gameObject, uint32_t id, const std::string& name)
    : Pot::Component(gameObject, id, name),
      Pot::IUpdateReceiver(id),
      Pot::IKeyboardReceiver(id),
      Pot::IMouseReceiver(id),
      navigating(false),
      transSpeed(10),
      rotSpeed(0.3),
      direction(0,0,0)
{
}

CameraController::~CameraController()
{
}

void CameraController::initialize()
{
}

void CameraController::finish()
{
}

void CameraController::onUpdate(double deltaTime)
{
    auto& inputMgr = Pot::Engine::getInstance().getInputManager();
    auto transform = gameObject->getComponent<Pot::Components::Transform>();

    // Rotate camera accoriding to mouse delta
    int du, dv;
    std::tie(du, dv) = inputMgr.getMouseDelta();

    // Move along local XZ plane
    glm::vec4 u = transform->getWorldMatrix() * glm::vec4(direction, 0);

    if(navigating)
    {
        transform->localRot.y -= rotSpeed * du * deltaTime;
        transform->localRot.x -= rotSpeed * dv * deltaTime;
        transform->localPos += float(deltaTime) * transSpeed * glm::vec3(u);
    }
}

void CameraController::onKeyUp()
{
    checkNavigation();
    recomputeDirection();
}

void CameraController::onKeyDown()
{
    checkNavigation();
    recomputeDirection();
}

void CameraController::onMouseMove()
{
    recomputeDirection();
}

void CameraController::setSpeed(float speed)
{
    if(speed <= 0)
        THROW_EXCEPTION("Speed must be positive, " << speed << " supplied");
    this->transSpeed = speed;
}

void CameraController::checkNavigation()
{
    auto& inputMgr = Pot::Engine::getInstance().getInputManager();
    bool nowNavigating = inputMgr.isKeyPressed(SDL_Scancode::SDL_SCANCODE_LCTRL);

    if(!navigating && nowNavigating)
        Pot::Engine::getInstance().getInputManager().lockMouse();
    else if(navigating && !nowNavigating)
        Pot::Engine::getInstance().getInputManager().unlockMouse();

    navigating = nowNavigating;
}

void CameraController::recomputeDirection()
{
    // Compute direction vector relative to origin
    auto& inputMgr = Pot::Engine::getInstance().getInputManager();

    bool wPressed = inputMgr.isKeyPressed(SDL_Scancode::SDL_SCANCODE_W);
    bool sPressed = inputMgr.isKeyPressed(SDL_Scancode::SDL_SCANCODE_S);
    bool aPressed = inputMgr.isKeyPressed(SDL_Scancode::SDL_SCANCODE_A);
    bool dPressed = inputMgr.isKeyPressed(SDL_Scancode::SDL_SCANCODE_D);

    direction.x = 0;
    direction.z = 0;

    // Forward-backward
    if(aPressed)
        direction.x = -1;
    else if(dPressed)
        direction.x = +1;

    // Left-right
    if(wPressed)
        direction.z = -1;
    else if(sPressed)
        direction.z = +1;
}

}

}
