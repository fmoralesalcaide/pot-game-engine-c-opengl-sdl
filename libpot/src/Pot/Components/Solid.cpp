#include <Pot/GameObject.h>
#include <Pot/Components/Transform.h>
#include <Pot/Components/Solid.h>

namespace Pot
{

namespace Components
{

Solid::Solid(Pot::GameObject* gameObject, uint32_t id, const std::string& name)
    : Pot::Component(gameObject, id, name),
      Pot::IUpdateReceiver(id),
      Pot::ICollisionReceiver(id, gameObject->getId()),
      receivesGravity(true),
      receivesImpacts(true),
      mass(1),
      gravityAccel(0,-9.8,0),
      impactAccel(0,0,0),
      speed(0,0,0)
{
}

Solid::~Solid()
{
}

void Solid::onUpdate(double deltaTime)
{
    // Compute kinetics
    glm::vec3 totalAccel(0,0,0);

    if(receivesGravity)
        totalAccel += gravityAccel;
    if(receivesImpacts)
        totalAccel += impactAccel;

    speed += totalAccel * float(deltaTime);

    // Apply kinetics to Transform
    Pot::Components::Transform* transform = gameObject->getComponent<Pot::Components::Transform>();

    glm::mat4 inverseWorldMatrix = glm::inverse(transform->getWorldMatrix());

    glm::vec3 v(inverseWorldMatrix * glm::vec4(speed, 0));

    transform->localPos += 0.5f * v * float(deltaTime);
}

void Solid::onCollisionEnter(const Pot::CollisionInfo& info)
{
}

void Solid::onCollisionStay(const Pot::CollisionInfo& info)
{
}

void Solid::onCollisionLeave()
{
}

bool Solid::hasGravity() const
{
    return receivesGravity;
}

bool Solid::canImpact() const
{
    return receivesImpacts;
}

double Solid::getMass() const
{
    return mass;
}

const glm::vec3& Solid::getGravityAccel() const
{
    return gravityAccel;
}

const glm::vec3& Solid::getImpactAccel() const
{
    return impactAccel;
}

glm::vec3 Solid::getTotalAccel() const
{
    return gravityAccel + impactAccel;
}

const glm::vec3& Solid::getSpeed() const
{
    return speed;
}

void Solid::setHasGravity(bool hasGravity)
{
    this->receivesGravity = hasGravity;
}

void Solid::setCanImpact(bool canImpact)
{
    receivesImpacts = canImpact;
}

void Solid::setMass(double mass)
{
    if(mass <= 0)
        THROW_EXCEPTION("Solid mass value must be a positive in Kg units");
    this->mass = mass;
}

void Solid::setGravityAccel(const glm::vec3& gravity)
{
    this->gravityAccel = gravity;
}

}

}

