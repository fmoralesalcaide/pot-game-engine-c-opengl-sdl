#include <Pot/Components/MeshRenderer.h>
#include <Pot/Components/Transform.h>
#include <Pot/Engine.h>
#include <Pot/GameObject.h>
#include <Pot/Managers/CameraManager.h>
#include <Pot/Resources/MeshResource.h>

namespace Pot
{

namespace Components
{

MeshRenderer::MeshRenderer(GameObject* gameObject, uint32_t id, const std::string& name)
    : Component(gameObject, id, name),
      IRenderReceiver(id),
      hasShaderAttached(false),
      mesh(nullptr),
      shaderProgramId(0)
{
}

MeshRenderer::~MeshRenderer()
{
}

void MeshRenderer::setShader(const std::string& shaderName)
{
    shaderProgramId = Engine::getInstance().getRenderManager().getShaderDescriptor(shaderName);
    hasShaderAttached = true;
}

void MeshRenderer::setMesh(const std::string& name)
{
    ResourceManager& resourceMgr = Engine::getInstance().getResourceManager();
    mesh = resourceMgr.getResource(Resource::Type::MESH, name);
}

void MeshRenderer::onRender()
{
    if(!hasShaderAttached || !mesh)
        return;

    // Get pointers to buffer objects
    GLuint positionBuffer = Engine::getInstance().getRenderManager().getPositionBuffer();
    GLuint normalBuffer = Engine::getInstance().getRenderManager().getNormalBuffer();
    GLuint uvBuffer = Engine::getInstance().getRenderManager().getUVBuffer();
    GLuint elementBuffer = Engine::getInstance().getRenderManager().getElementBuffer();

    // Set active shader
    glUseProgram(shaderProgramId);

    // Configure vertex attributes
    GLint positionId = glGetAttribLocation(shaderProgramId, "position");
    GLint normalId = glGetAttribLocation(shaderProgramId, "normal");
    GLint uvId = glGetAttribLocation(shaderProgramId, "uv");

    glEnableVertexAttribArray(positionId);
    glBindBuffer(GL_ARRAY_BUFFER, positionBuffer);
    glVertexAttribPointer(positionId, 3, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0));

    glEnableVertexAttribArray(normalId);
    glBindBuffer(GL_ARRAY_BUFFER, normalBuffer);
    glVertexAttribPointer(normalId, 3, GL_FLOAT, GL_TRUE, 0, BUFFER_OFFSET(0));

    glEnableVertexAttribArray(uvId);
    glBindBuffer(GL_ARRAY_BUFFER, uvBuffer);
    glVertexAttribPointer(uvId, 2, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0));

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementBuffer);

    // Configure uniforms
    GLint modelMatrixId = glGetUniformLocation(shaderProgramId, "modelMatrix");
    GLint viewProjMatrixId = glGetUniformLocation(shaderProgramId, "viewProjMatrix");

    bool includeScaleMatrix = true;
    const glm::mat4& modelMatrix = gameObject->getComponent<Transform>()->getWorldMatrix(includeScaleMatrix);
    const glm::mat4& viewProjMatrix = Engine::getInstance().getCameraManager().getActiveCamera()->getViewProjMatrix();

    glUniformMatrix4fv(modelMatrixId, 1, GL_FALSE, &modelMatrix[0][0]);
    glUniformMatrix4fv(viewProjMatrixId, 1, GL_FALSE, &viewProjMatrix[0][0]);

    // Draw mesh
    Engine::getInstance().getRenderManager().renderMesh(mesh);

    // Disable vertex attribute arrays
    glDisableVertexAttribArray(positionId);
    glDisableVertexAttribArray(normalId);
    glDisableVertexAttribArray(uvId);
}

}

}
