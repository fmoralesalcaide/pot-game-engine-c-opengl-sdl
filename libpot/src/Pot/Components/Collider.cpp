#include <Pot/Components/Collider.h>
#include <Pot/Engine.h>
#include <Pot/Managers/CollisionManager.h>
#include <Pot/Components/Transform.h>

namespace Pot
{

namespace Components
{

Collider::Collider(Type type, GameObject* gameObject, uint32_t id, const std::string& name)
    : Component(gameObject, id, name),
      type(type),
      relativePos(0,0,0)
{
    Engine::getInstance().getCollisionManager().registerCollider(id, this);
}

Collider::~Collider()
{
    Engine::getInstance().getCollisionManager().unregisterCollider(id);
}

bool Collider::isUniqueType()
{
    return false;
}

void Collider::setRelativePosition(const glm::vec3& relativePos)
{
    this->relativePos = relativePos;
}

glm::vec3 Collider::getWorldPosition()
{
    Components::Transform* transform = gameObject->getComponent<Components::Transform>();
    return transform->getWorldPosition();
}

}

}
