#include <Pot/Components/Camera.h>
#include <Pot/Components/Transform.h>
#include <Pot/Engine.h>
#include <Pot/GameObject.h>
#include <Pot/Managers/CameraManager.h>

namespace Pot
{

namespace Components
{

Camera::Camera(Type type, GameObject* gameObject, uint32_t id, const std::string& name)
    : Component(gameObject, id, name),
      type(type),
      recomputeProjMatrix(true)
{
    Engine::getInstance().getCameraManager().registerCamera(id, this);
}

Camera::~Camera()
{
    Engine::getInstance().getCameraManager().unregisterCamera(id);
}

bool Camera::isUniqueType()
{
    return false;
}

void Camera::setActive()
{
    Engine::getInstance().getCameraManager().setActiveCamera(id);
}

void Camera::updateViewProjMatrix()
{
    Transform* transform = gameObject->getComponent<Transform>();

    if(recomputeProjMatrix)
    {
        projMatrix = computeProjectionMatrix();
        recomputeProjMatrix = false;
    }

    viewProjMatrix = projMatrix * transform->getViewMatrix();
}

}

}

