#include <Pot/Components/Cameras/PerspectiveCamera.h>

namespace Pot
{

namespace Components
{

PerspectiveCamera::PerspectiveCamera(GameObject* gameObject, uint32_t id, const std::string& name)
    : Camera(Camera::Type::PERSPECTIVE, gameObject, id, name),
      fieldOfView(75),
      aspectRatio(4.0f / 3.0f),
      nearClippingPlane(0.1),
      farClippingPlane(1000)
{
}

PerspectiveCamera::~PerspectiveCamera()
{
}

void PerspectiveCamera::setFieldOfView(double fieldOfView)
{
    if(this->fieldOfView == fieldOfView)
        return;

    if(fieldOfView < 0)
        THROW_EXCEPTION("Field of view value must be nonnegative, " << fieldOfView << " supplied");
    if(fieldOfView > 180)
        THROW_EXCEPTION("Field of view value must be < 180 degrees, " << fieldOfView << " supplied");

    this->fieldOfView = fieldOfView;
    recomputeProjMatrix = true;
}

void PerspectiveCamera::setAspectRatio(double aspectRatio)
{
    if(this->aspectRatio == aspectRatio)
        return;

    if(aspectRatio <= 0)
        THROW_EXCEPTION("Aspect ratio value must be positive, " << aspectRatio << " supplied");

    this->aspectRatio = aspectRatio;
    recomputeProjMatrix = true;
}

void PerspectiveCamera::setNearClippingPlane(double nearClippingPlane)
{
    if(this->nearClippingPlane == nearClippingPlane)
        return;

    if(nearClippingPlane >= farClippingPlane)
        THROW_EXCEPTION("Near clipping plane value must be lower than far clipping plane (" << farClippingPlane << "), " << nearClippingPlane << " supplied");

    this->nearClippingPlane = nearClippingPlane;
    recomputeProjMatrix = true;
}

void PerspectiveCamera::setFarClippingPlane(double farClippingPlane)
{
    if(this->farClippingPlane == farClippingPlane)
        return;

    if(farClippingPlane <= nearClippingPlane)
        THROW_EXCEPTION("Far clipping plane value must be greater than near clipping plane (" << nearClippingPlane << "), " << farClippingPlane << " supplied");

    this->farClippingPlane = farClippingPlane;
    recomputeProjMatrix = true;
}

glm::mat4 PerspectiveCamera::computeProjectionMatrix()
{
    return glm::perspective(glm::radians(fieldOfView), aspectRatio, nearClippingPlane, farClippingPlane);
}

}

}
