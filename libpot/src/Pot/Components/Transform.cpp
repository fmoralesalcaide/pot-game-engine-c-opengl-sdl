#include <Pot/Components/Transform.h>
#include <Pot/GameObject.h>

namespace Pot
{

namespace Components
{

Transform::Transform(GameObject* gameObject, uint32_t id, const std::string& name)
    : Component(gameObject, id, name),
      scale(1, 1, 1),
      localRot(0, 0, 0),
      localPos(0, 0, 0),
      lastScale(0, 0, 0),
      lastRot(1, 1, 1),
      lastPos(1, 1, 1)
{
}

Transform::~Transform()
{
}

void Transform::initialize()
{
    recomputeMatrices();
}
/*
void Transform::lookAt(const glm::vec3& target)
{
	// Rotate towards target first
	glm::mat4 invRot = glm::lookAt(glm::vec3(0), target - pos, glm::vec3(0, 1, 0));
	rotMatrix = rotMatrix * glm::inverse(invRot);

	// Update euler angles
	rot = glm::eulerAngles(glm::quat_cast(rotMatrix));
}
*/

void Transform::rotateLocally(const glm::vec3& v)
{
    // Perform local rotation first
    glm::mat4 rotLocal = glm::toMat4(glm::quat(v));
    rotMatrix = rotMatrix * rotLocal;

    // Update euler angles
    localRot = glm::eulerAngles(glm::quat_cast(rotMatrix));
}

void Transform::translateLocally(const glm::vec3& v)
{
    // Obtain the translation point in world co-ordinates
    localPos = glm::vec3(rotTransMatrix * glm::vec4(v, 1));

    // Update translation matrix
    glm::mat4 m(1.0f);
    transMatrix = glm::translate(m, localPos);
}

const glm::mat4& Transform::getViewMatrix()
{
    bool needRecompute = (lastRot != localRot) || (lastPos != localPos);

    if(needRecompute)
        recomputeMatrices();

    return viewMatrix;
}

const glm::mat4& Transform::getRotTransMatrix()
{
    bool needRecompute = (lastRot != localRot) || (lastPos != localPos);

    if(needRecompute)
        recomputeMatrices();

    return rotTransMatrix;
}


const glm::mat4& Transform::getModelMatrix()
{
    bool needRecompute = (lastScale != scale) || (lastRot != localRot) || (lastPos != localPos);

    if(needRecompute)
        recomputeMatrices();

    return modelMatrix;
}

glm::mat4 Transform::getWorldMatrix(bool includeScaleMatrix)
{
    bool needRecompute = (lastRot != localRot) || (lastPos != localPos);

    if(needRecompute)
        recomputeMatrices();

    const glm::mat4& matrix = includeScaleMatrix ? modelMatrix : rotTransMatrix;

    if(!gameObject->hasParent())
        return matrix;
    else
    {
        GameObject* parent = gameObject->getParent();

        if(!parent->hasComponent<Transform>())
            THROW_EXCEPTION("Parent of GameObject with Transform component must also have a Transform component");

        return parent->getComponent<Transform>()->getWorldMatrix() * matrix;
    }
}

glm::vec3 Transform::getWorldPosition()
{
	// Return translation vector from world matrix
	return glm::vec3(getWorldMatrix()[3]);
}

void Transform::recomputeMatrices()
{
    glm::mat4 m(1.0f);

    bool scaleChanged = (lastScale != scale);
    bool rotChanged = (lastRot != localRot);
    bool posChanged = (lastPos != localPos);

    if(scaleChanged)
    {
        scaleMatrix = glm::scale(m, scale);
        lastScale = scale;
    }

    if(rotChanged)
    {
        rotMatrix = glm::toMat4(glm::quat(localRot));
        lastRot = localRot;
    }

    if(posChanged)
    {
        transMatrix = glm::translate(m, localPos);
        lastPos = localPos;
    }

    if(rotChanged || posChanged)
    {
        rotTransMatrix = transMatrix * rotMatrix;
        viewMatrix = glm::inverse(rotTransMatrix);
    }

    modelMatrix = rotTransMatrix * scaleMatrix;
}

}

}
