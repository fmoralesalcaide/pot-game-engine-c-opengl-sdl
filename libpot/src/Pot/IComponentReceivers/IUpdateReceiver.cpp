#include <Pot/IComponentReceivers/IUpdateReceiver.h>
#include <Pot/Engine.h>
#include <Pot/Managers/UpdateManager.h>

namespace Pot
{

IUpdateReceiver::IUpdateReceiver(uint32_t componentId)
    : IComponentReceiver(componentId)
{
    Engine::getInstance().getUpdateManager().registerReceiver(componentId, this);
}

IUpdateReceiver::~IUpdateReceiver()
{
    Engine::getInstance().getUpdateManager().unregisterReceiver(componentId);
}

void IUpdateReceiver::onUpdate(double deltaTime)
{
    // Nothing by default
}

}
