#include <Pot/IComponentReceivers/IKeyboardReceiver.h>
#include <Pot/Engine.h>
#include <Pot/Managers/InputManager.h>

namespace Pot
{

IKeyboardReceiver::IKeyboardReceiver(uint32_t componentId)
    : IComponentReceiver(componentId)
{
    Engine::getInstance().getInputManager().registerKeyboardReceiver(componentId, this);
}

IKeyboardReceiver::~IKeyboardReceiver()
{
    Engine::getInstance().getInputManager().unregisterKeyboardReceiver(componentId);
}

void IKeyboardReceiver::onKeyUp()
{
    // Nothing by default
}

void IKeyboardReceiver::onKeyDown()
{
    // Nothing by default
}
}
