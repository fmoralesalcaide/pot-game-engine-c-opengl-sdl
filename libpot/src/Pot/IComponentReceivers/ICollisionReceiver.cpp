#include <Pot/IComponentReceivers/ICollisionReceiver.h>
#include <Pot/Engine.h>
#include <Pot/GameObject.h>
#include <Pot/Managers/CollisionManager.h>

namespace Pot
{

ICollisionReceiver::ICollisionReceiver(uint32_t componentId, uint32_t gameObjectId)
    : IComponentReceiver(componentId),
      gameObjectId(gameObjectId)
{
    Engine::getInstance().getCollisionManager().registerCollisionReceiver(componentId, gameObjectId, this);
}

ICollisionReceiver::~ICollisionReceiver()
{
    Engine::getInstance().getCollisionManager().unregisterCollisionReceiver(componentId, gameObjectId);
}

void ICollisionReceiver::onCollisionEnter(const CollisionInfo& info)
{
    // Nothing by default
}

void ICollisionReceiver::onCollisionStay(const CollisionInfo& info)
{
    // Nothing by default
}

void ICollisionReceiver::onCollisionLeave()
{
    // Nothing by default
}

}
