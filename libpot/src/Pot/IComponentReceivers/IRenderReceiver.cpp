#include <Pot/IComponentReceivers/IRenderReceiver.h>
#include <Pot/Engine.h>
#include <Pot/Managers/RenderManager.h>

namespace Pot
{

IRenderReceiver::IRenderReceiver(uint32_t componentId)
    : IComponentReceiver(componentId)
{
    Engine::getInstance().getRenderManager().registerReceiver(componentId, this);
}

IRenderReceiver::~IRenderReceiver()
{
    Engine::getInstance().getRenderManager().unregisterReceiver(componentId);
}

void IRenderReceiver::onRender()
{
    // Nothing by default
}

}
