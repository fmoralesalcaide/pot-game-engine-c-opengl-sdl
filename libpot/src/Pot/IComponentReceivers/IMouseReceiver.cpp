#include <Pot/IComponentReceivers/IMouseReceiver.h>
#include <Pot/Engine.h>
#include <Pot/Managers/InputManager.h>

namespace Pot
{

IMouseReceiver::IMouseReceiver(uint32_t componentId)
    : IComponentReceiver(componentId)
{
    Engine::getInstance().getInputManager().registerMouseReceiver(componentId, this);
}

IMouseReceiver::~IMouseReceiver()
{
    Engine::getInstance().getInputManager().unregisterMouseReceiver(componentId);
}

void IMouseReceiver::onMouseUp()
{
    // Nothing by default
}

void IMouseReceiver::onMouseDown()
{
    // Nothing by default
}

void IMouseReceiver::onMouseMove()
{
    // Nothing by default
}

void IMouseReceiver::onMouseScroll()
{
    // Nothing by default
}

}
