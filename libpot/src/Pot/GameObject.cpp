#include <Pot/Engine.h>
#include <Pot/GameObject.h>

namespace Pot
{
GameObject::GameObject(uint32_t id)
    : id(id),
      parent(nullptr)
{
}

GameObject::~GameObject()
{
    if(!children.empty())
        THROW_EXCEPTION("Children GameObject list found when destroying GameObject");
    if(!components.empty())
        THROW_EXCEPTION("Component list found when destroying GameObject");
}

bool GameObject::hasParent()
{
    return parent;
}

bool GameObject::hasChild(const uint32_t& id)
{
    return children.count(id);
}

bool GameObject::hasChildren()
{
    return !children.empty();
}


GameObject* GameObject::getParent()
{
    if(!hasParent())
        THROW_EXCEPTION("GameObject (" << id << ") does not have a parent");
    return parent;
}

GameObject* GameObject::getChild(const uint32_t& id)
{
    if(!hasChild(id))
        THROW_EXCEPTION("GameObject (" << this->id << ") does not have child (" << id << ")");
    return children.at(id);
}

std::vector<GameObject*> GameObject::getChildren()
{
    std::vector<GameObject*> childVector;

    for(auto it : children)
        childVector.push_back(it.second);

    return childVector;
}

void GameObject::detachFromParent()
{
    if(!hasParent())
        THROW_EXCEPTION("Cannot detach GameObject (" << id << ") from un-existing parent");
    GameObject* tmpParentPointer = parent;
    parent = nullptr;
    if(tmpParentPointer->hasChild(id))
        tmpParentPointer->detachChild(id);
}

void GameObject::detachChild(const uint32_t& id)
{
    if(!children.count(id))
        THROW_EXCEPTION("Cannot detach unattached child GameObject (" << id << ") from parent GameObject (" << this->id << ")");
    GameObject* tmpChildPointer = children.at(id);
    children.erase(id);
    if(tmpChildPointer->hasParent())
        tmpChildPointer->detachFromParent();
}

void GameObject::detachChildren()
{
    while(!children.empty())
        detachChild(children.begin()->first);
}

void GameObject::attachToParent(GameObject* newParent)
{
    if(!newParent)
        THROW_EXCEPTION("Cannot attach GameObject (" << id << ") to a null parent");
    if(hasParent())
        THROW_EXCEPTION("GameObject (" << id << ") is already attached to a parent");
    parent = newParent;
    if(!parent->hasChild(id))
        parent->attachChild(this);
}

void GameObject::attachChild(GameObject* newChild)
{
    if(!newChild)
        THROW_EXCEPTION("Cannot attach null child to GameObject (" << id << ")");
    if(hasChild(newChild->getId()))
        THROW_EXCEPTION("GameObject (" << newChild->getId() << ") is already attached to the specified parent");
    children[newChild->getId()] = newChild;
    if(!newChild->hasParent())
        newChild->attachToParent(this);
}

void GameObject::destroy()
{
    Engine::getInstance().getGameObjectManager().addToDeletionList(this);
}

std::vector<Component*> GameObject::getAllComponents() const
{
    std::vector<Component*> componentVector;

    for(auto it : components)
        componentVector.push_back(it.second);

    return componentVector;
}

std::vector<size_t> GameObject::getComponentTypeIds() const
{
    std::vector<size_t> typeIds;

    for(auto it : componentsByType)
        typeIds.push_back(it.first);

    return typeIds;
}

void GameObject::notifyAllComponentsRemoved()
{
    componentsByType.clear();
    components.clear();
}
}
