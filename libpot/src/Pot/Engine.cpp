#include <Pot/Engine.h>

namespace Pot
{
Engine::Engine()
    : running(false)
{
}

void Engine::initialize()
{
    inputMgr.initialize();
    collisionMgr.initialize();
    renderMgr.initialize();
    resourceMgr.initialize();
    cameraMgr.initialize();
    updateMgr.initialize();
    gameObjectMgr.initialize();
    componentMgr.initialize();
}

void Engine::run()
{
    running = true;
    auto lastTime = std::chrono::high_resolution_clock::now();

    while(running)
    {
        auto currentTime(std::chrono::high_resolution_clock::now());
        auto elapsed = std::chrono::duration_cast<std::chrono::nanoseconds>(currentTime - lastTime);
        double deltaTime = elapsed.count() * 1e-9;

        // Mouse and keyboard events
        inputMgr.processInput();

        // Collision events
        collisionMgr.processCollisions();

        // Update game logic
        updateMgr.update(deltaTime);

        // Process active camera
        cameraMgr.process();

        // Render visual elements
        renderMgr.render();

        // Clean up flagged gameobjects
        gameObjectMgr.processDeletionList();

        lastTime = currentTime;
    }
}

void Engine::stop()
{
    running = false;
}

void Engine::finish()
{
    componentMgr.finish();
    gameObjectMgr.finish();
    updateMgr.finish();
    cameraMgr.finish();
    resourceMgr.finish();
    renderMgr.finish();
    collisionMgr.finish();
    inputMgr.finish();
}

}
