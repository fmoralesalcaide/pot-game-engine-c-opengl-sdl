# Pot Engine #

Pot Engine is a lightweight C++ game engine developed in C++, based on:

* SDL2
* OpenGL
* Apache XML Xerces
* Boost system

Pot Engine is implemented as a single shared library (libpot.so).

# Dependencies #

The following library dependencies are needed for Pot Engine to compile:

* libsdl2-dev
* libglew-dev
* libgl1-mesa-dev
* libglm-dev
* libxerces-c-dev
* libboost-system-dev

-l SDL2 GLEW GL xerces-c boost_system
